-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- ----------
-- General --
-- ----------

DELETE FROM mob_spawn_mods WHERE mobid = "17428751" AND modid = 55; -- Remove Sozu Rogberry 5 minutes idle despawn from old force pop system.
DELETE FROM mob_spawn_mods WHERE mobid = "17424444" AND modid = 55; -- Remove Pallas 5 minutes idle despawn from old force pop system.
DELETE FROM mob_spawn_mods WHERE mobid = "17424480" AND modid = 55; -- Remove Alkyoneus 5 minutes idle despawn from old force pop system.
DELETE FROM mob_spawn_mods WHERE mobid = "16814361" AND modid = 55; -- Remove Nunyunuwi 5 minutes idle despawn as is not era (https://ffxiclopedia.fandom.com/wiki/Nunyunuwi)
DELETE FROM mob_spawn_mods WHERE mobid = "17031401" AND modid = 55; -- Remove Big Bomb 5 minutes idle despawn as is not era
DELETE from mob_spawn_mods WHERE mobid = "17506418" and modid = 55; -- Removing Ullikummi 5 minutes idle despawn as is not era


