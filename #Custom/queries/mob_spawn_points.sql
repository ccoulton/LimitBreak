-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- ----------
-- General --
-- ----------

-- The Shrine of Ru'Avitau
UPDATE mob_spawn_points SET pos_x = "739.973", pos_y = "0.400" , pos_z = "-99.555", pos_rot ="1" WHERE mobid = "17506370"; -- Fix faust default pos and rotation.

-- Mammet-19_Epsilon
UPDATE mob_spawn_points SET pos_x = -600.000, pos_y = 82.200, pos_z = 6.176, pos_rot = 253 WHERE mobid = "16904193"; -- Repositioned Mammet-19_Epsilon for era Accuracy
UPDATE mob_spawn_points SET pos_x = -600.000, pos_y = 82.200, pos_z = -6.176, pos_rot = 253 WHERE mobid = "16904194"; -- Repositioned Mammet-19_Epsilon for era Accuracy
UPDATE mob_spawn_points SET pos_x = -600.000, pos_y = 82.200, pos_z = 0.000, pos_rot = 253 WHERE mobid = "16904195"; -- Repositioned Mammet-19_Epsilon for era Accuracy
UPDATE mob_spawn_points SET pos_x = 0.000, pos_y = 2.200, pos_z = 6.176, pos_rot = 253 WHERE mobid = "16904196"; -- Repositioned Mammet-19_Epsilon for era Accuracy
UPDATE mob_spawn_points SET pos_x = 0.000, pos_y = 2.200, pos_z = -6.176, pos_rot = 253 WHERE mobid = "16904197"; -- Repositioned Mammet-19_Epsilon for era Accuracy
UPDATE mob_spawn_points SET pos_x = 0.000, pos_y = 2.200, pos_z = 0.000, pos_rot = 253 WHERE mobid = "16904198"; -- Repositioned Mammet-19_Epsilon for era Accuracy
UPDATE mob_spawn_points SET pos_x = 600.000, pos_y = -77.800, pos_z = 6.176, pos_rot = 253 WHERE mobid = "16904199"; -- Repositioned Mammet-19_Epsilon for era Accuracy
UPDATE mob_spawn_points SET pos_x = 600.000, pos_y = -77.800, pos_z = -6.176, pos_rot = 253 WHERE mobid = "16904200"; -- Repositioned Mammet-19_Epsilon for era Accuracy
UPDATE mob_spawn_points SET pos_x = 600.000, pos_y = -77.800, pos_z = 0.000, pos_rot = 253 WHERE mobid = "16904201"; -- Repositioned Mammet-19_Epsilon for era Accuracy

-- Balga's Dais
UPDATE mob_spawn_points SET pos_x = -140.667, pos_y = 55.587, pos_z = -215.541 WHERE mobid = "17375443"; -- Giant Moa - Moa Constrictors KSNM
UPDATE mob_spawn_points SET pos_x = -144.130, pos_y = 55.662, pos_z = -216.312 WHERE mobid = "17375444"; -- Giant Moa - Moa Constrictors KSNM
UPDATE mob_spawn_points SET pos_x = 18.489, pos_y = -3.855, pos_z = -20.360 WHERE mobid = "17375446"; -- Giant Moa - Moa Constrictors KSNM
UPDATE mob_spawn_points SET pos_x = 16.118, pos_y = -4.307, pos_z = -19.686 WHERE mobid = "17375447"; -- Giant Moa - Moa Constrictors KSNM
UPDATE mob_spawn_points SET pos_x = 0.000, pos_y = 0.000, pos_z = 0.000 WHERE mobid = "17375449"; -- Giant Moa - Moa Constrictors KSNM
UPDATE mob_spawn_points SET pos_x = 0.000, pos_y = 0.000, pos_z = 0.000 WHERE mobid = "17375450"; -- Giant Moa - Moa Constrictors KSNM

-- Ghelsba Outpost
UPDATE mob_spawn_points SET pos_x = -184.000, pos_y = -10.000, pos_z = 45.000, pos_rot = 95 WHERE mobid = "17350933"; -- Kalamainu - Petrifying Pair BCNM
UPDATE mob_spawn_points SET pos_x = -193.000, pos_y = -10.000, pos_z = 55.000, pos_rot = 95 WHERE mobid = "17350934"; -- Kilioa - Petrifying Pair BCNM

-- Horlais Peak
UPDATE mob_spawn_points SET pos_x = -399.000, pos_z = -67.000 WHERE mobid = "17346606"; -- Pilwiz - Carapace Combatants BCNM


-- Jade Sepulcher
INSERT IGNORE INTO `mob_spawn_points` VALUES (17051673,'Raubahn','Raubahn',2270,243,-31,236,159); -- The Beast Within - BLU LB5
INSERT IGNORE INTO `mob_spawn_points` VALUES (17051674,'Raubahn','Raubahn',2270,243,-31,236,159); -- The Beast Within - BLU LB5
INSERT IGNORE INTO `mob_spawn_points` VALUES (17051675,'Raubahn','Raubahn',2270,243,-31,236,159); -- The Beast Within - BLU LB5

-- Korroloka Tunnel
UPDATE mob_spawn_points SET pos_x = -212.056, pos_y = -10.185, pos_z = 178.804 WHERE mobname = "Korroloka_Leech" and mobid = 17486187; -- Slightly repositioned for era accuracy
UPDATE mob_spawn_points SET pos_x = -206.117, pos_y = -11.701, pos_z = 179.106 WHERE mobname = "Korroloka_Leech" and mobid = 17486188; -- Slightly repositioned for era accuracy
UPDATE mob_spawn_points SET pos_x = -204.902, pos_y = -11.361, pos_z = 173.605 WHERE mobname = "Korroloka_Leech" and mobid = 17486189; -- Slightly repositioned for era accuracy

-- Mount Zhayolm
UPDATE mob_spawn_points SET groupid = 56 WHERE mobname = "Wamoura" and mobid = 17027421;  -- Adding missing Wamoura
UPDATE mob_spawn_points SET groupid = 56 WHERE mobname = "Wamoura" and mobid = 17027422;  -- Adding missing Wamoura


-- Quicksand Caves
UPDATE mob_spawn_points SET pos_x = -4.000 WHERE mobname = "Centurio_V-III" and mobid = 17465345; -- ZM6 mobs should be more spread out
UPDATE mob_spawn_points SET pos_x = 4.000 WHERE mobname = "Triarius_V-VIII" and mobid = 17465346; -- ZM6 mobs should be more spread out
UPDATE mob_spawn_points SET pos_x = -3.975 WHERE mobname = "Centurio_V-III" and mobid = 17465348; -- ZM6 mobs should be more spread out
UPDATE mob_spawn_points SET pos_x = 4.025 WHERE mobname = "Triarius_V-VIII" and mobid = 17465349; -- ZM6 mobs should be more spread out
UPDATE mob_spawn_points SET pos_x = -3.945 WHERE mobname = "Centurio_V-III" and mobid = 17465351; -- ZM6 mobs should be more spread out
UPDATE mob_spawn_points SET pos_x = 4.055 WHERE mobname = "Triarius_V-VIII" and mobid = 17465352; -- ZM6 mobs should be more spread out

UPDATE mob_spawn_points SET groupid = 39, pos_x = 672.4598, pos_y = 8.0002, pos_z = -617.2017 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629255; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 658.8737, pos_y = -2.044, pos_z = -653.5038 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629256; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 601.4016, pos_y = -5.9207, pos_z = -689.1025 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629261; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 589.2606, pos_y = -5.6367, pos_z = -671.8018 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629262; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 40, pos_x = 602.7662, pos_y = -5.9207, pos_z = -680.8004 WHERE mobname = "Sabotender_Bailarin" and mobid = 17629264; -- Added Sabotender_Bailarin positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 540.6154, pos_y = -11.1537, pos_z = -703.0149 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629265; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 697.7896, pos_y = 9.0821, pos_z = -593.6597 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629266; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 513.1458, pos_y = 16.245, pos_z = -939.8481 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629312; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 500.2155, pos_y = 8.1004, pos_z = -912.6168 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629313; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 470.8165, pos_y = 1.0202, pos_z = -900.7175 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629314; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 461.4344, pos_y = 1.3031, pos_z = -871.3561 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629340; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 432.1423, pos_y = -0.0406, pos_z = -858.7776 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629341; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 492.8908, pos_y = 1.1991, pos_z = -859.9655 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629343; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 16.6305, pos_y = 0, pos_z = -196.6782 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629345; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 4.4869, pos_y = -0.0125, pos_z = -219.7973 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629347; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = -26.7504, pos_y = -0.0448, pos_z = -220.134 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629348; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = -18.7748, pos_y = 0.0071, pos_z = -190.9411 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629353; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = -301.1938, pos_y = -6.9023, pos_z = -118.0092 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629354; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = -275.6811, pos_y = -7.0357, pos_z = -99.8262 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629386; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = -248.9263, pos_y = -0.3998, pos_z = -58.7341 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629406; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = -218.9742, pos_y = 5.3132, pos_z = -60.4696 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629407; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = -195.5847, pos_y = 9.6246, pos_z = -103.4818 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629451; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = -212.8878, pos_y = 9.6687, pos_z = -133.536 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629456; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = -181.7648, pos_y = 8.8859, pos_z = -126.5184 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629457; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = -178.2305, pos_y = 12.8125, pos_z = -176.9764 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629459; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = -164.9852, pos_y = 16.2978, pos_z = -184.8389 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629460; -- Added Sabotender_Bailaor positions

-- Talacca Cove
INSERT IGNORE INTO `mob_spawn_points` VALUES (17010722,'Qultada','Qultada',1980,-180,39,187,61); -- Breaking the Bonds of Fate - Cor LB5
INSERT IGNORE INTO `mob_spawn_points` VALUES (17010723,'Qultada','Qultada',1980,-180,39,187,61); -- Breaking the Bonds of Fate - Cor LB5
INSERT IGNORE INTO `mob_spawn_points` VALUES (17010724,'Qultada','Qultada',1980,-180,39,187,61); -- Breaking the Bonds of Fate - Cor LB5

-- Uleguerand Range
UPDATE mob_spawn_points SET mobname = "Mountain_Worm_NM" WHERE mobid = "16798031"; -- This NM needs a unique name as it shares his with regular worms in Ule. Otherwise it can't be scripted.

-- Zeruhn Mines
INSERT IGNORE INTO `mob_spawn_points` VALUES (17482751,'Giant_Amoeba','Giant Amoeba',8,60.425,8.735,-263.460,181); -- Adding Giant Amoeba NM

-- CoP 5-3U Where Messengers Gather (Boneyard Gully Fight)
UPDATE mob_spawn_points SET pos_x = -565.048, pos_y = 2.755, pos_z = -431.988, pos_rot = 64 WHERE mobid = "16809985"; -- Shikaree_X Arena 1
UPDATE mob_spawn_points SET pos_x = -570.200, pos_y = 3.572, pos_z = -431.988, pos_rot = 64 WHERE mobid = "16809987"; -- Shikaree_Z Arena 1
UPDATE mob_spawn_points SET pos_x = -9.625, pos_y = 3.432, pos_z = 129.045, pos_rot = 64 WHERE mobid = "16809992"; -- Shikaree_X Arena 2
UPDATE mob_spawn_points SET pos_x = -4.473, pos_y = 2.615, pos_z = 129.045, pos_rot = 64 WHERE mobid = "16809990"; -- Shikaree_Z Arena 2
UPDATE mob_spawn_points SET pos_x = 469.424, pos_y = 3.639, pos_z = 611.211, pos_rot = 64 WHERE mobid = "16809997"; -- Shikaree_X Arena 3
UPDATE mob_spawn_points SET pos_x = 474.576, pos_y = 2.822, pos_z = 611.211, pos_rot = 64 WHERE mobid = "16809995"; -- Shikaree_Z Arena 3

-- Spires of Holla
UPDATE mob_spawn_points SET pos_rot = 64 WHERE mobname = "Wreaker"; -- Set all rotations to face forward

-- Ordelles Caves
INSERT IGNORE INTO `mob_spawn_points` VALUES (17568767,'Stroper_Chyme','Stroper Chyme',32,-22.736,28.725,-75.709,64); -- Added 3rd Stroper Chyme
INSERT IGNORE INTO `mob_spawn_points` VALUES (17568766,'Stroper_Chyme','Stroper Chyme',32,-141.509,28.725,-75.709,64); -- Added 4th Stroper Chyme

-- BCNM 20 Wings of Fury
UPDATE mob_spawn_points SET pos_x = -188.2127, pos_y = -10.1180, pos_z = 47.2502, pos_rot = 100 WHERE mobid = "17350929"; -- Colo-colo
UPDATE mob_spawn_points SET pos_x = -182.1980, pos_y = -9.9389, pos_z = 48.6278, pos_rot = 100 WHERE mobid = "17350930"; -- Furies
UPDATE mob_spawn_points SET pos_x = -187.0657, pos_y = -10.4493, pos_z = 53.0073, pos_rot = 100 WHERE mobid = "17350931"; -- Furies

-- SAM AF3
UPDATE mob_spawn_points SET pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 1 WHERE mobname = "Ayakashi";

-- Divine Punishers BCNM
	-- Arena 1
UPDATE mob_spawn_points SET pos_x = -135.5, pos_y = 56.5, pos_z = -224, pos_rot = 190 WHERE mobid = "17375365"; -- Voo_Tolu_the_Ghostfist 1
UPDATE mob_spawn_points SET pos_x = -139.0, pos_y = 56.5, pos_z = -224, pos_rot = 190 WHERE mobid = "17375369"; -- Aa_Nawu_the_Thunderblade 1
UPDATE mob_spawn_points SET pos_x = -142.5, pos_y = 56.5, pos_z = -224, pos_rot = 190 WHERE mobid = "17375370"; -- Yoo_Mihi_the_Haze 1
UPDATE mob_spawn_points SET pos_x = -135.5, pos_y = 56.5, pos_z = -226, pos_rot = 190 WHERE mobid = "17375366"; -- Cuu_Doko_the_Blizzard 1
UPDATE mob_spawn_points SET pos_x = -139.0, pos_y = 56.5, pos_z = -226, pos_rot = 190 WHERE mobid = "17375368"; -- Gii_Jaha_the_Raucous 1
UPDATE mob_spawn_points SET pos_x = -142.5, pos_y = 56.5, pos_z = -226, pos_rot = 190 WHERE mobid = "17375367"; -- Zuu_Xowu_the_Darksmoke 1
	-- Arena 2
UPDATE mob_spawn_points SET pos_x = 24.5, pos_y = -3.5, pos_z = -22.7, pos_rot = 190 WHERE mobid = "17375372"; -- Voo_Tolu_the_Ghostfist 2
UPDATE mob_spawn_points SET pos_x = 21.0, pos_y = -3.5, pos_z = -22.7, pos_rot = 190 WHERE mobid = "17375376"; -- Aa_Nawu_the_Thunderblade 2
UPDATE mob_spawn_points SET pos_x = 17.5, pos_y = -3.5, pos_z = -22.7, pos_rot = 190 WHERE mobid = "17375377"; -- Yoo_Mihi_the_Haze 2
UPDATE mob_spawn_points SET pos_x = 24.5, pos_y = -3.5, pos_z = -25.0, pos_rot = 190 WHERE mobid = "17375373"; -- Cuu_Doko_the_Blizzard 2
UPDATE mob_spawn_points SET pos_x = 21.0, pos_y = -3.5, pos_z = -25.0, pos_rot = 190 WHERE mobid = "17375375"; -- Gii_Jaha_the_Raucous 2
UPDATE mob_spawn_points SET pos_x = 17.5, pos_y = -3.5, pos_z = -25.0, pos_rot = 190 WHERE mobid = "17375374"; -- Zuu_Xowu_the_Darksmoke 2
	-- Arena 3
UPDATE mob_spawn_points SET pos_x = 184.5, pos_y = -63.5, pos_z = 177.0, pos_rot = 190 WHERE mobid = "17375379"; -- Voo_Tolu_the_Ghostfist 3
UPDATE mob_spawn_points SET pos_x = 181.0, pos_y = -63.5, pos_z = 177.0, pos_rot = 190 WHERE mobid = "17375383"; -- Aa_Nawu_the_Thunderblade 3
UPDATE mob_spawn_points SET pos_x = 177.5, pos_y = -63.5, pos_z = 177.0, pos_rot = 190 WHERE mobid = "17375384"; -- Yoo_Mihi_the_Haze 3
UPDATE mob_spawn_points SET pos_x = 184.5, pos_y = -63.5, pos_z = 174.5, pos_rot = 190 WHERE mobid = "17375380"; -- Cuu_Doko_the_Blizzard 3
UPDATE mob_spawn_points SET pos_x = 181.0, pos_y = -63.5, pos_z = 174.5, pos_rot = 190 WHERE mobid = "17375382"; -- Gii_Jaha_the_Raucous 3
UPDATE mob_spawn_points SET pos_x = 177.5, pos_y = -63.5, pos_z = 174.5, pos_rot = 190 WHERE mobid = "17375381"; -- Zuu_Xowu_the_Darksmoke 3

-- Promy Holla Zone Rework
UPDATE mob_spawn_points SET pos_x = 104.9824, pos_y = 0, pos_z = -238.7194, pos_rot = 53 WHERE mobid = "16842996";
UPDATE mob_spawn_points SET pos_x = 104.6884, pos_y = 0, pos_z = -236.7257, pos_rot = 99 WHERE mobid = "16843010";
UPDATE mob_spawn_points SET pos_x = -4.4, pos_y = 0, pos_z = 166.9161, pos_rot = 22 WHERE mobid = "16842762";
UPDATE mob_spawn_points SET pos_x = -6.8225, pos_y = 0, pos_z = 155.3888, pos_rot = 30 WHERE mobid = "16842761";
UPDATE mob_spawn_points SET pos_x = -31.1769, pos_y = 0, pos_z = 128.5388, pos_rot = 61 WHERE mobid = "16842763";
UPDATE mob_spawn_points SET pos_x = -207.4494, pos_y = 0, pos_z = -85.8353, pos_rot = 122 WHERE mobid = "16842829";
UPDATE mob_spawn_points SET pos_x = -214.4471, pos_y = 0, pos_z = -92.8047, pos_rot = 60 WHERE mobid = "16842811";
UPDATE mob_spawn_points SET pos_x = -219.357, pos_y = 0, pos_z = -72.8066, pos_rot = 217 WHERE mobid = "16842810";
UPDATE mob_spawn_points SET pos_x = 202.3691, pos_y = 0, pos_z = -180.1865, pos_rot = 56 WHERE mobid = "16843023";
UPDATE mob_spawn_points SET pos_x = 58.102, pos_y = 0, pos_z = 297.2291, pos_rot = 52 WHERE mobid = "16842921";
UPDATE mob_spawn_points SET pos_x = 168.0189, pos_y = 0, pos_z = 338.3727, pos_rot = 0 WHERE mobid = "16842935";
UPDATE mob_spawn_points SET pos_x = 161.086, pos_y = 0, pos_z = -207.6107, pos_rot = 241 WHERE mobid = "16843015";
UPDATE mob_spawn_points SET pos_x = 159.2483, pos_y = 0, pos_z = -195.7994, pos_rot = 202 WHERE mobid = "16843013";
UPDATE mob_spawn_points SET pos_x = 277.5939, pos_y = 0, pos_z = -150.8496, pos_rot = 102 WHERE mobid = "16843026";
UPDATE mob_spawn_points SET pos_x = 251.4929, pos_y = 0, pos_z = -82.5969, pos_rot = 125 WHERE mobid = "16843031";
UPDATE mob_spawn_points SET pos_x = 211.6537, pos_y = 0, pos_z = -50.2975, pos_rot = 100 WHERE mobid = "16843032";
UPDATE mob_spawn_points SET pos_x = 217.7825, pos_y = 0, pos_z = -40.2925, pos_rot = 80 WHERE mobid = "16843033";
UPDATE mob_spawn_points SET pos_x = 124.7702, pos_y = 0, pos_z = -41.2027, pos_rot = 30 WHERE mobid = "16843034";
UPDATE mob_spawn_points SET pos_x = 2.6838, pos_y = 0, pos_z = -185.5633, pos_rot = 119 WHERE mobid = "16843039";
UPDATE mob_spawn_points SET pos_x = -6.7434, pos_y = 0, pos_z = -174.3297, pos_rot = 213 WHERE mobid = "16842808";
UPDATE mob_spawn_points SET pos_x = -7.31, pos_y = 0, pos_z = -203.4486, pos_rot = 162 WHERE mobid = "16842807";
UPDATE mob_spawn_points SET pos_x = 88.0521, pos_y = 0, pos_z = -95.7068, pos_rot = 126 WHERE mobid = "16842998";

-- Promy Dem Zone Rework
UPDATE mob_spawn_points SET pos_x = -281.4977, pos_y = 0, pos_z = -147.937, pos_rot = 194 WHERE mobid = "16851007";
UPDATE mob_spawn_points SET pos_x = 28.5979, pos_y = 0, pos_z = -242.4267, pos_rot = 90 WHERE mobid = "16851111";
UPDATE mob_spawn_points SET pos_x = 38.9762, pos_y = 0, pos_z = -249.727, pos_rot = 30 WHERE mobid = "16851115";
UPDATE mob_spawn_points SET pos_x = -224.9102, pos_y = 0, pos_z = 298.0087, pos_rot = 53 WHERE mobid = "";
UPDATE mob_spawn_points SET pos_x = -281.4455, pos_y = 0, pos_z = -148.3793, pos_rot = 180 WHERE mobid = "16851021";
UPDATE mob_spawn_points SET pos_x = 65.9505, pos_y = 0, pos_z = 75.2799, pos_rot = 250 WHERE mobid = "16851234";
UPDATE mob_spawn_points SET pos_x = 72.0093, pos_y = 0, pos_z = 109.3414, pos_rot = 170 WHERE mobid = "16851237";
UPDATE mob_spawn_points SET pos_x = 105.1381, pos_y = 0, pos_z = 145.7895, pos_rot = 218 WHERE mobid = "16851238";
UPDATE mob_spawn_points SET pos_x = 92.5125, pos_y = 0, pos_z = 113.3645, pos_rot = 175 WHERE mobid = "16851241";
UPDATE mob_spawn_points SET pos_x = 158.7571, pos_y = 0, pos_z = -260.9823, pos_rot = 50 WHERE mobid = "16850965";
UPDATE mob_spawn_points SET pos_x = 152.3412, pos_y = 0, pos_z = -274.252, pos_rot = 105 WHERE mobid = "16850964";
UPDATE mob_spawn_points SET pos_x = -241.0292, pos_y = 0, pos_z = 321.1828, pos_rot = 214 WHERE mobid = "16851106";
UPDATE mob_spawn_points SET pos_x = -230.8023, pos_y = 0, pos_z = 329.0938, pos_rot = 180 WHERE mobid = "16851103";
UPDATE mob_spawn_points SET pos_x = -159.966, pos_y = 0, pos_z = 253.1593, pos_rot = 64 WHERE mobid = "16851120";
UPDATE mob_spawn_points SET pos_x = -135.457, pos_y = 0, pos_z = 255.6546, pos_rot = 60 WHERE mobid = "16851116";
UPDATE mob_spawn_points SET pos_x = -39.9547, pos_y = 0, pos_z = 197.9651, pos_rot = 180 WHERE mobid = "16851128";
UPDATE mob_spawn_points SET pos_x = 137.2684, pos_y = 0, pos_z = 144.2999, pos_rot = 13 WHERE mobid = "16851233";
UPDATE mob_spawn_points SET pos_x = 131.069, pos_y = 0, pos_z = 210.4403, pos_rot = 203 WHERE mobid = "16851182";

-- Promy Mea Zone Rework
UPDATE mob_spawn_points SET pos_x = -7.9353, pos_y = 0, pos_z = 205.5068, pos_rot = 173 WHERE mobid = "16859403";
UPDATE mob_spawn_points SET pos_x = 15.6043, pos_y = 0, pos_z = 211.0029, pos_rot = 239 WHERE mobid = "16859405";
UPDATE mob_spawn_points SET pos_x = 32.8088, pos_y = 0, pos_z = 233.1867, pos_rot = 60 WHERE mobid = "16859414";
UPDATE mob_spawn_points SET pos_x = 18.6954, pos_y = 0, pos_z = 268.584, pos_rot = 90 WHERE mobid = "16859432";
UPDATE mob_spawn_points SET pos_x = -264.5457, pos_y = 0, pos_z = 15.9821, pos_rot = 214 WHERE mobid = "16859239";
UPDATE mob_spawn_points SET pos_x = -56.3249, pos_y = 0, pos_z = 29.2449, pos_rot = 152 WHERE mobid = "16859245";
UPDATE mob_spawn_points SET pos_x = -39.6501, pos_y = 0, pos_z = 37.2909, pos_rot = 148 WHERE mobid = "16859235";
UPDATE mob_spawn_points SET pos_x = -66.8032, pos_y = 0, pos_z = 53.6121, pos_rot = 101 WHERE mobid = "16859237";
UPDATE mob_spawn_points SET pos_x = 51.262, pos_y = 0, pos_z = 225.4719, pos_rot = 113 WHERE mobid = "16859400";
UPDATE mob_spawn_points SET pos_x = 0.3369, pos_y = 0, pos_z = 205.426, pos_rot = 82 WHERE mobid = "16859440";
UPDATE mob_spawn_points SET pos_x = -11.6207, pos_y = 0, pos_z = 272.4629, pos_rot = 13 WHERE mobid = "16859406";
UPDATE mob_spawn_points SET pos_x = -5.8938, pos_y = 0, pos_z = 290.7134, pos_rot = 165 WHERE mobid = "16859439";
UPDATE mob_spawn_points SET pos_x = 28.1076, pos_y = 0, pos_z = 281.0533, pos_rot = 88 WHERE mobid = "16859424";
UPDATE mob_spawn_points SET pos_x = 63.3434, pos_y = 0, pos_z = 231.1371, pos_rot = 220 WHERE mobid = "16859418";

-- Bostaunieux Oubliette
UPDATE mob_spawn_points SET pos_x = -21.04, pos_y = 0.9251, pos_z = -340.1736, pos_rot = 127 WHERE mobid = "17461307";

-- --------------------
-- Removing OOE NMs --
-- --------------------

-- Alzadaal Undersea Ruins
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Wulgaru"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Oupire"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Ob"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Cookieduster_Lipiroon"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17072150"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Cheese_Hoarder_Gigiroon"; -- 2008

-- Arrapago Reef
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Zareehkl_the_Jubilant"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Velionis"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Nuhn"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Lil_Apkallu"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Euryale"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Boompadu"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Armed_Gears"; -- 2008

-- Attohwa Chasm
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Sekhmet"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Sargas"; -- 2009

-- Aydeewa_Subterrane
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Pandemonium_Warden"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Nosferatu"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Lizardtrap"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Chigre"; -- 2008

-- Batallia Downs
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Skirling_Liger"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17207410"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Prankster_Maverix"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17207640"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Eyegouger"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17207608"; -- 2009

-- Beaucedine Glacier
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Calcabrina"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17232117"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Humbaba"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17232094"; -- 2009

-- Bhaflau Thickets
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Nis_Puk"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "16990403"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Mahishasura"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "16990306"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Lividroot_Amooshah"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Harvestman"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "16990252"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Dea"; -- 2008

-- Bibiki Bay
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Shankha"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "16793698"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Splacknuck";
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "16793776"; -- 2009

-- Buburimu Peninsula
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Wake_Warder_Wanda"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17260732"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Backoo"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0, pos_rot = 0 WHERE mobname = "Ketos"; -- 2009

-- Caedarva_Mire
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Vidhuwa_the_Wrathborn"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Verdelet"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Tyger"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Mahjlaef_the_Paintorn"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Experimental_Lamia"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Aynu-kaysey"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17101099"; -- 2009

-- Cape Teriggan
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Zmey_Gorynych"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Tegmine"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Killer_Jonny"; -- 2009

-- Carpenters Landing
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Tempest_Tigon"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "16785593"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Splacknuck"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "16793776"; -- 2009

-- Castle Oztroja
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Lii_Jixa_the_Somnolist"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Saa_Doyi_the_Fervid"; -- 2009

-- Castle Zvahl Baileys
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Marquis_Sabnock"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17436881"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Marquis_Naberius"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Likho"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17436714"; -- 2009

-- Crawlers Nest
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Dynast_Beetle"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17584312"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Aqrabuamelu"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17584416"; -- 2009

-- Dangruf Wadi
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Teporingo"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17559584"; -- 2009

-- East Ronfaure
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Rambukk"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17191044"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Quagmire Pugil"; -- 2010
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Sunderclaw"; -- 2010

-- East Sarutabaruta
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Duke_Decapod"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17252725"; -- 2009

-- Eastern Altepa Desert
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Sabotender_Corrido"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Nandi"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17244471"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Donnergugi"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17244268"; -- 2009

-- Fei'Yin
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Sluagh"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Mind_Hoarder"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17612859"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Jenglot"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17612840"; -- 2009

-- Fort Ghelsba
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Kegpaunch_Doshgnosh"; -- 2009

-- Garlaige Citadel
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Hovering_Hotpot"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17596628"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Hazmat"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17596520"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Frogamander"; -- 2009

-- Giddeus
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Quu_Xijo_the_Illusory"; -- 2009

-- Halvung
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Reacton"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Flammeri"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Dextrose"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Copper_Borer"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Achamoth"; -- 2008

-- Inner Horutoto Ruins
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Slendlix_Spindlethumb"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17563785"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Nocuous_Weapon"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17563801"; -- 2009

-- Jugner Forest
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Supplespine_Mujwuj"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17203475"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Sappy_Sycamore"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17203547"; -- 2009

-- King Ranperres Tomb
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Gwyllgi"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17555664"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Barbastelle"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17555721"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Ankou"; -- 2009

-- Konschtat Highlands
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Highlander_Lizard"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17219787"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Ghillie_Dhu"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17219619"; -- 2009

-- Korroloka Tunnel
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Thoon"; -- 2009

-- La Theine Plateau
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Void_Hare"; -- 2010
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Prickly_Sheep"; -- 2010
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Slumbering_Samwell"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17195221"; -- 2009

-- Lower Delkfutts Tower
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Tyrant"; -- 2009

-- Lufaise Meadows
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Flockbock"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Sengann"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Yal-un_Eke"; -- 2009

-- Mamook
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Venomfang"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Iriri_Samariri"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Firedance_Magmaal_Ja"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17043779"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Chamrosh"; -- 2008

-- Maze of Shakhrami
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Trembler_Tabitha"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17588278"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Gloombound_Lurker"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Lesath"; -- 2009

-- Meriphataud Mountains
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Chonchon"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Naa_Zeku_the_Unwaiting"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17264768"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Patripatan"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17264972"; -- 2009

-- Misareaux Coast
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Goaftrap"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Okyupete"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "16879847"; -- 2009

-- Mount Zhayolm
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Sarameya"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Khromasoul_Bhurborlor"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Fahrafahr_the_Bloodied"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Ignamoth"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17027423"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Claret"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Anantaboga"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Brass_Borer"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Chary_Apkallu"; -- 2009

-- Newton Movalpolos
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Sword_Sorcerer_Solisoq"; -- 2009

-- North Gustaberg
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Bedrock_Barry"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17211666"; -- 2009

-- Oldton Movalpolos
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Bugbear_Muscleman"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "16797770"; -- 2009

-- Ordelles Caves
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Agar_Agar"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17567901"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Donggu"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17567801"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Bombast"; -- 2009

-- Outer Horutoto Ruins
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Desmodont"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17571870"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Ah_Puch"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17571903"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Legalox_Heftyhind"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17571873"; -- 2009

-- Palborough Mines
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "BeHya_Hundredwall"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17363258"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "QuVho_Deathhurler"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17363080"; -- 2009

-- Pashhow Marshlands
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "NiZho_Bladebender"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17223797"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Toxic_Tamlyn"; -- 2009

-- Qufim Island
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Qoofim"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Atkorkamuy"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Slippery_Sucker"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17293389"; -- 2009

-- Ranguemont Pass
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Hyakume"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Gloom_Eye"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17457204"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Mucoid_Mass"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17457245"; -- 2009

-- Rolanberry Fields
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Eldritch_Edge"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17228150"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Ravenous_Crawler"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17228086"; -- 2009

-- RoMaeve
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Rogue_Receptacle"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17277079"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Nargun"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Martinet"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17277011"; -- 2009

-- Sauromugue Champaign
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Blighting_Brand"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17269016"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Thunderclaw_Thuban"; -- 2009

-- South Gustaberg
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Tococo"; -- 2009

-- Tahrongi Canyon
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Herbage_Hunter"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17256836"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Habrok"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17256493"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Bashe"; -- 2009

-- The Sanctuary of ZiTah
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Huwasi"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17272958"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Elusive_Edwin"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17272915"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Bastet"; -- 2009

-- Toraimarai Canal
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Konjac"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17469632"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Brazen_Bones"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Canal_Moocher"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17469578"; -- 2009

-- Uleguerand Range
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Frost_Flambeau"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Magnotaur"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "16797968"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Skvader"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "16797770"; -- 2009

-- Upper Delkfutts Tower
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Autarch"; -- 2009

-- Valkurm_Dunes
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Hippomaritimus"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17199351"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Metal_Shears"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17199161"; -- 2009

-- Wajaom Woodlans
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Vulpangue"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Tinnin"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Iriz_Ima"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Gotoh_Zha_the_Redolent"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Gharial"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "16986320"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Chelicerata"; -- 2009

-- West Ronfaure
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Amanita"; -- 2009

-- Yhoator Jungle
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Powderer_Penny"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17285248"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Hoar-knuckled_Rimberry"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17285394"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Acolnahuacatl"; -- 2009

-- Yuhtunga Jungle
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Pyuu_the_Spatemaker"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Koropokkur"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17281061"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Bayawak"; -- 2009

-- Wajaom Woodlands
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "16986355"; -- Deactivating Hydra until reworked. DELETE THIS ENTRY ONCE HYDRA IS FIXED!

-- West Ronfaure
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Pyracmon";

-- West Sarutabaruta
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Numbing_Norman"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17248588"; -- 2009

-- Western Altepa Desert
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Picolaton"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17289638"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Dahu"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Calchas"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17289547"; -- 2009

-- Xarcabard
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Timeworn_Warrior"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17236045"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Duke_Focalor"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17236146"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Barbaric_Weapon"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17236027"; -- 2009

-- --------------------------------
-- Cleaning over populated areas --
-- --------------------------------

-- NOTE: this is a known DSP/Topaz issue as retail mob pool and spawn system is not fully accurate


-- Maze of Shakhrami

/*  COMMENTING THIS OUT FOR WINGS AS SEEMS ALREADY FIX BUT IT AFFECTS TOPAZ-NEXT DB
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588449; -- Poison Leech
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588690; -- Poison Leech
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588693; -- Poison Leech
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588686; -- Poison Leech
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588679; -- Poison Leech
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588680; -- Poison Leech
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588678; -- Poison Leech
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588689; -- Poison Leech
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588692; -- Poison Leech
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588327; -- Ghoul
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588326; -- Ghoul
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588612; -- Wendigo
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588660; -- Goblin Shaman
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588659; -- Globin Smithy
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588663; -- Globin Furrier
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588632; -- Globin Shaman
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588658; -- Goblin Furrier
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588655; -- Goblin Shaman
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588694; -- Protozoan
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588665; -- Goblin Shaman
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588672; -- Labyrinth Scorpion
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588638; -- Ancient Bat
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588639; -- Ancient Bat
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588586; -- Labyrinth Scorpion
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588625; -- Ancient Bat
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588596; -- Goblin Mugger
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588592; -- Goblin Gambler
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588597; -- Goblin Leecher
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588507; -- Stink Bats
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588545; -- Wight
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588577; -- Wight
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588531; -- Wight
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588537; -- Ancient Bat
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588527; -- Wight
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588504; -- Goblin Ambusher
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588508; -- Stink Bat
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588498; -- Wight
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588521; -- Abyss Worm
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588581; -- Wight
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588518; -- Abyss Worm
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588576; -- Wight
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588505; -- Goblin Tinkerer
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588511; -- Goblin Leecher
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588494; -- Stink Bats
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588535; -- Abyss Worm
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588533; -- Abyss Worm
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588522; -- Abyss Worm
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588580; -- Wight
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588503; -- Goblin Butcher
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588517; -- Abyss Worm
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588470; -- Ghoul
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588302; -- Goblin Butcher
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588300; -- Goblin Ambusher
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588308; -- Goblin Leecher
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588312; -- Goblin Gambler
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588307; -- Goblin Mugger
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588587; -- Protozoan
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588544; -- Wight 
*/

-- Monastic Cavern
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391621; -- Orcish Warchief
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391639; -- Orcish Zerker
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391640; -- Orcish Warchief
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391623; -- Orcish Gladiator
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391620; -- Orcish Trooper
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391618; -- Orcish Footsoldier
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391631; -- Orcish Zerker
UPDATE mob_spawn_points SET pos_x = -20.4147, pos_y = -0.1545, pos_z = -357.7487 WHERE mobid = 17391624; -- Orcish Trooper
UPDATE mob_spawn_points SET pos_x = -22.9977, pos_y = -1.6404, pos_z = -368.0098 WHERE mobid = 17391632; -- Orcish Warchief
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391685; -- Orcish Zerker
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391686; -- Orcish Warchief
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391687; -- Orcish Veteran
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391693; -- Orcish Veteran
UPDATE mob_spawn_points SET pos_x = -22.2975, pos_y = -0.6960, pos_z = -212.0731 WHERE mobid = 17391691; -- Orcish Warchief
UPDATE mob_spawn_points SET pos_x = 25.4334, pos_y = -0.5305, pos_z = -219.0916 WHERE mobid = 17391692; -- Orcish Dreadnought
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391697; -- Orcish Veteran
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391694; -- Orcish Predator
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391699; -- Orcish Zerker
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391702; -- Orcish Predator
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391738; -- Orcish Champion
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391737; -- Orcish Dreadnought
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391719; -- Orcish Dreadnought
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391721; -- Orcish Dragoon
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391724; -- Orcish Champion
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391730; -- Orcish Champion
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391733; -- Orcish Dreadnought
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391728; -- Orcish Farkiller
UPDATE mob_spawn_points SET pos_x = -41.9276, pos_y = -0.1078, pos_z = -139.0336 WHERE mobid = 17391705; -- Orcish Champion
UPDATE mob_spawn_points SET pos_x = -63.2345, pos_y = -0.4325, pos_z = -232.5098 WHERE mobid = 17391670; -- Orcish Dreadnought
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391673; -- Orcish Zerker
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391671; -- Orcish Veteran
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391676; -- Orcish Predator
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391664; -- Orcish Warchief
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391657; -- Orcish Veteran
UPDATE mob_spawn_points SET pos_x = -57.2369, pos_y = -0.3426, pos_z = -262.1214 WHERE mobid = 17391658; -- Orcish Dreadnought
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391648; -- Orcish Warchief
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391647; -- Orcish Zerker
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391641; -- Orcish Veteran
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391655; -- Orcish
UPDATE mob_spawn_points SET pos_x = 55.5556, pos_y = -0.4180, pos_z = -259.9413 WHERE mobid = 17391682; -- Orcish Warchief
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391747; -- Orcish Dragoon
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391759; -- Orcish Dragoon
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391746; -- Orcish Champion
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391740; -- Orcish Farkiller
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391752; -- Orcish Farkiller
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391753; -- Orcish Dreadnought
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391748; -- Orcish Farkiller
UPDATE mob_spawn_points SET pos_x = 218.0535, pos_y = -0.5788, pos_z = -148.6448 WHERE mobid = 17391757; -- Orcish Warchief
UPDATE mob_spawn_points SET pos_x = 216.7683, pos_y = -9.1406, pos_z = -238.7831 WHERE mobid = 17391750; -- Orcish Warchief
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391799; -- Orcish Footsoldier
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391798; -- Orcish Bowshooter
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391790; -- Orcish Gladiator
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391792; -- Orcish Predator
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391785; -- Orcish Footsoldier
-- UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = xxxxxx; -- xxxx