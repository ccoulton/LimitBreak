-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- ----------
-- GENERAL --
-- ----------

UPDATE traits SET meritid = "2116" WHERE name = "invigorate"; -- Fixes issue where non-merited job traits appear under "Abilities/Job Traits" when you reach 75
UPDATE traits SET meritid = "2118" WHERE name = "penance"; -- Fixes issue where non-merited job traits appear under "Abilities/Job Traits" when you reach 75
UPDATE traits SET meritid = "2564" WHERE name = "beast affinity"; -- Fixes issue where non-merited job traits appear under "Abilities/Job Traits" when you reach 75
UPDATE traits SET meritid = "2566" WHERE name = "beast healer"; -- Fixes issue where non-merited job traits appear under "Abilities/Job Traits" when you reach 75
UPDATE traits SET meritid = "2692" WHERE name = "snapshot"; -- Fixes issue where non-merited job traits appear under "Abilities/Job Traits" when you reach 75
UPDATE traits SET meritid = "2694" WHERE name = "recycle"; -- Fixes issue where non-merited job traits appear under "Abilities/Job Traits" when you reach 75
UPDATE traits SET meritid = "2884" WHERE name = "empathy"; -- Fixes issue where non-merited job traits appear under "Abilities/Job Traits" when you reach 75
UPDATE traits SET meritid = "2886" WHERE name = "strafe"; -- Fixes issue where non-merited job traits appear under "Abilities/Job Traits" when you reach 75
UPDATE traits SET meritid = "3012" WHERE name = "enchainment"; -- Fixes issue where non-merited job traits appear under "Abilities/Job Traits" when you reach 75
UPDATE traits SET meritid = "3014" WHERE name = "assimilation"; -- Fixes issue where non-merited job traits appear under "Abilities/Job Traits" when you reach 75
UPDATE traits SET meritid = "3076" WHERE name = "winning streak"; -- Fixes issue where non-merited job traits appear under "Abilities/Job Traits" when you reach 75
UPDATE traits SET meritid = "3078" WHERE name = "loaded deck"; -- Fixes issue where non-merited job traits appear under "Abilities/Job Traits" when you reach 75
UPDATE traits SET meritid = "3140" WHERE name = "fine-tuning"; -- Fixes issue where non-merited job traits appear under "Abilities/Job Traits" when you reach 75
UPDATE traits SET meritid = "3274" WHERE name = "stormsurge"; -- Fixes issue where non-merited job traits appear under "Abilities/Job Traits" when you reach 75
UPDATE traits SET meritid = "2052" WHERE name = "savagery"; -- Fixes issue where non-merited job traits appear under "Abilities/Job Traits" when you reach 75
UPDATE traits SET meritid = "2054" WHERE name = "aggressive aim"; -- Fixes issue where non-merited job traits appear under "Abilities/Job Traits" when you reach 75


-- Warrior
UPDATE traits SET level = "15" WHERE name = "resist virus" AND rank = "1" AND job = "1"; -- Min level was wrong in Topaz


-- Monk
UPDATE traits SET value = "10" WHERE name = "counter" AND rank = "1" AND job = "2"; -- Has an 8% by default but couldn't find data on that. Adjusted to 10% increase as in retail