-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- ----------
-- GENERAL --
-- ----------

UPDATE mob_family_system SET HP = 90 WHERE familyid = 258; -- Adjusting Worm base HP value (was too high)
UPDATE mob_family_system SET HP = 90 WHERE familyid = 276; -- Adjusting BigWorm base HP value (was too high)

UPDATE mob_family_system SET system = "Demon" WHERE familyid = 224; -- Change Shadow Lord family system to "Demon" classification (currently unclassified)
UPDATE mob_family_system SET system = "Demon" WHERE familyid = 225; -- Change Shadow Lord family system to "Demon" classification (currently unclassified)
UPDATE mob_family_system SET system = "Demon" WHERE familyid = 361; -- Change Dynamis Lord family system to "Demon" classification (currently unclassified)

-- Adding custom Goblin family so Goblin Fishers can drop Lightning Crystals
-- INSERT IGNORE INTO mob_family_system VALUES (111,'Goblin',7,'Beastmen',0,40,91,120,1,3,5,3,4,4,4,3,3,3,3,1,1,1,1,1,1,1,1,1,1,1.25,1,5,1,0); --NOT NEEDED FOR WINGS, ACTUALLY THIS NEEDS UPDATE FOR TOPAZ-NEXT AS SHOULD AFFECT ALL ARMORED GOBLINS

-- Cardians should drop up to 4 different crystal types
INSERT IGNORE INTO `mob_family_system` VALUES(550, 'Cardian', 3, 'Arcana', 0, 40, 109, 140, 4, 3, 4, 5, 1, 3, 4, 3, 3, 3, 3, 1, 1, 1, 1, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 4, 34, 0); -- Earth type
INSERT IGNORE INTO `mob_family_system` VALUES(551, 'Cardian', 3, 'Arcana', 0, 40, 109, 140, 4, 3, 4, 5, 1, 3, 4, 3, 3, 3, 3, 1, 1, 1, 1, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 7, 34, 0); -- Light type
INSERT IGNORE INTO `mob_family_system` VALUES(552, 'Cardian', 3, 'Arcana', 0, 40, 109, 140, 4, 3, 4, 5, 1, 3, 4, 3, 3, 3, 3, 1, 1, 1, 1, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1.3, 1, 34, 0); -- Fire type

-- King Behemoth
UPDATE mob_family_system SET speed = 60 WHERE family = "King_Behemoth"; -- Bigger model and faster speed