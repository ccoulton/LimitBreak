-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- These will update the default values for NEW characters. Any characters that already have slots --
-- will be unaffected. THIS MUST BE DONE BEFORE ANY CHARACTERS ARE CREATED!!!!!!                   --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- ----------
-- General --
-- ----------

ALTER TABLE char_storage CHANGE COLUMN satchel satchel TINYINT(2) UNSIGNED NOT NULL DEFAULT 0 ;
ALTER TABLE char_storage CHANGE COLUMN `case` `case` TINYINT(2) UNSIGNED NOT NULL DEFAULT 0 ;
ALTER TABLE char_storage CHANGE COLUMN wardrobe wardrobe TINYINT(2) UNSIGNED NOT NULL DEFAULT 0 ;
ALTER TABLE char_storage CHANGE COLUMN wardrobe2 wardrobe2 TINYINT(2) UNSIGNED NOT NULL DEFAULT 0 ;
ALTER TABLE char_storage CHANGE COLUMN wardrobe3 wardrobe3 TINYINT(2) UNSIGNED NOT NULL DEFAULT 0 ;
ALTER TABLE char_storage CHANGE COLUMN wardrobe4 wardrobe4 TINYINT(2) UNSIGNED NOT NULL DEFAULT 0 ;

