-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- -----------
-- GENERAL  --
-- -----------

UPDATE mob_droplist SET itemRate = "50" WHERE itemID = "901" AND dropType != 2  AND dropType != 4  AND dropID  = "72"; -- *venomous_claw* from *Amikiri* was *42*

-- Arrapago_Reef
UPDATE mob_groups SET minLevel = 72, maxLevel = 73 WHERE name = "Dweomershell" AND zoneid = 54;

-- Bostaunieux Oubliette
UPDATE mob_groups SET minLevel = 55, maxLevel = 59 WHERE name = "Blind_Bat" AND zoneid = 167;
UPDATE mob_groups SET minLevel = 64, maxLevel = 66 WHERE name = "Dabilla" AND zoneid = 167;
UPDATE mob_groups SET minLevel = 58, maxLevel = 62 WHERE name = "Panna_Cotta" AND zoneid = 167;
UPDATE mob_groups SET minLevel = 68, maxLevel = 70 WHERE name = "Nachtmahr" AND zoneid = 167;
UPDATE mob_groups SET minLevel = 69, maxLevel = 74 WHERE name = "Wurdalak" AND zoneid = 167;

-- Bibiki Bay
UPDATE mob_groups SET minLevel = 76, maxLevel = 79 WHERE name = "hobgoblin_toreador" AND zoneid = 4;
UPDATE mob_groups SET minLevel = 76, maxLevel = 79 WHERE name = "hobgoblin_alastor" AND zoneid = 4;
UPDATE mob_groups SET minLevel = 76, maxLevel = 79 WHERE name = "hobgoblin_physician" AND zoneid = 4;
UPDATE mob_groups SET minLevel = 78, maxLevel = 80 WHERE name = "camelopard" AND zoneid = 4;
UPDATE mob_groups SET minLevel = 73, maxLevel = 76 WHERE name = "bight_rarab" AND zoneid = 4;
UPDATE mob_groups SET minLevel = 76, maxLevel = 78 WHERE name = "hypnos_eft" AND zoneid = 4;

-- Boyahda tree
UPDATE mob_groups SET minLevel = 72, maxLevel = 75 WHERE name = "Mourning_Crawler" AND zoneid = 153;
UPDATE mob_groups SET minLevel = 75, maxLevel = 78 WHERE name = "Snaggletooth_Peapuk" AND zoneid = 153;
UPDATE mob_groups SET minLevel = 73, maxLevel = 76 WHERE name = "Viseclaw" AND zoneid = 153;

-- Crawlers' Nest
UPDATE mob_groups SET minLevel = 50, maxLevel = 53 WHERE name = "Dancing_Jewel" AND zoneid = 197;
UPDATE mob_groups SET minLevel = 47, maxLevel = 49 WHERE name = "King_Crawler" AND zoneid = 197;
UPDATE mob_groups SET minLevel = 51, maxLevel = 54 WHERE name = "Olid_Funguar" AND zoneid = 197;
UPDATE mob_groups SET minLevel = 55, maxLevel = 57 WHERE name = "Vespo" AND zoneid = 197;

-- Dangruf_Wadi
UPDATE mob_groups SET minLevel = 21, maxLevel = 23 WHERE name = "Couloir_Leech" AND zoneid = 191;
UPDATE mob_groups SET minLevel = 16, maxLevel = 20 WHERE name = "Fume_Lizard" AND zoneid = 191;
UPDATE mob_groups SET minLevel = 9, maxLevel = 12 WHERE name = "Witchetty_Grub" AND zoneid = 191;
UPDATE mob_groups SET minLevel = 11, maxLevel = 14 WHERE name = "Prim_Pika" AND zoneid = 191;
UPDATE mob_groups SET minLevel = 16, maxLevel = 20 WHERE name = "Trimmer" AND zoneid = 191;
UPDATE mob_groups SET minLevel = 11, maxLevel = 14 WHERE name = "Natty_Gibbon" AND zoneid = 191;
UPDATE mob_groups SET minLevel = 12, maxLevel = 16 WHERE name = "Goblin_Headsman" AND zoneid = 191;
UPDATE mob_groups SET minLevel = 21, maxLevel = 23 WHERE name = "Goblin_Conjurer" AND zoneid = 191;
UPDATE mob_groups SET minLevel = 12, maxLevel = 16 WHERE name = "Goblin_Bladesmith" AND zoneid = 191;
UPDATE mob_groups SET minLevel = 21, maxLevel = 23 WHERE name = "Goblin_Bushwhacker" AND zoneid = 191;
UPDATE mob_groups SET minLevel = 12, maxLevel = 16 WHERE name = "Goblin_Brigand" AND zoneid = 191;
UPDATE mob_groups SET minLevel = 21, maxLevel = 23 WHERE name = "Goblin_Healer" AND zoneid = 191;
UPDATE mob_groups SET minLevel = 16, maxLevel = 20 WHERE name = "Fire_Elemental" AND zoneid = 191;
UPDATE mob_groups SET minLevel = 7, maxLevel = 9 WHERE name = "Wadi_Crab" AND zoneid = 191;
UPDATE mob_groups SET minLevel = 11, maxLevel = 14 WHERE name = "Wadi_Hare" AND zoneid = 191;
UPDATE mob_groups SET minLevel = 6, maxLevel = 8 WHERE name = "Hoarder_Hare" AND zoneid = 191;

-- FeiYin
UPDATE mob_groups SET minLevel = 40, maxLevel = 42 WHERE name = "Balayang" AND zoneid = 204;
UPDATE mob_groups SET minLevel = 41, maxLevel = 43 WHERE name = "Sentient_Carafe" AND zoneid = 204;
UPDATE mob_groups SET minLevel = 40, maxLevel = 42 WHERE name = "Wekufe" AND zoneid = 204;

-- Garlaige Citadel
UPDATE mob_groups SET minLevel = 56, maxLevel = 58 WHERE name = "Warden_Beetle" AND zoneid = 200;
UPDATE mob_groups SET minLevel = 59, maxLevel = 62 WHERE name = "Kaboom" AND zoneid = 200;
UPDATE mob_groups SET minLevel = 52, maxLevel = 55 WHERE name = "Fortalice_Bats" AND zoneid = 200;
UPDATE mob_groups SET minLevel = 54, maxLevel = 56 WHERE name = "Donjon_Bat" AND zoneid = 200;

-- Gusgen Mines
UPDATE mob_groups SET minLevel = 33, maxLevel = 36 WHERE name = "Accursed_Soldier" AND zoneid = 196;
UPDATE mob_groups SET minLevel = 33, maxLevel = 36 WHERE name = "Accursed_Sorcerer" AND zoneid = 196;
UPDATE mob_groups SET minLevel = 27, maxLevel = 30 WHERE name = "Madfly" AND zoneid = 196;
UPDATE mob_groups SET minLevel = 23, maxLevel = 26 WHERE name = "Rockmill" AND zoneid = 196;

-- Inner Horutoto Ruins
UPDATE mob_groups SET minLevel = 17, maxLevel = 20 WHERE name = "Covin_Bat" AND zoneid = 192;
UPDATE mob_groups SET minLevel = 11, maxLevel = 16 WHERE name = "Deathwatch_Beetle" AND zoneid = 192;
UPDATE mob_groups SET minLevel = 20, maxLevel = 23 WHERE name = "Goblin_Flesher" AND zoneid = 192;
UPDATE mob_groups SET minLevel = 20, maxLevel = 23 WHERE name = "Goblin_Lurcher" AND zoneid = 192;
UPDATE mob_groups SET minLevel = 20, maxLevel = 23 WHERE name = "Goblin_Metallurgist" AND zoneid = 192;
UPDATE mob_groups SET minLevel = 20, maxLevel = 23 WHERE name = "Goblin_Trailblazer" AND zoneid = 192;
UPDATE mob_groups SET minLevel = 25, maxLevel = 28 WHERE name = "Skinnymalinks" AND zoneid = 192;
UPDATE mob_groups SET minLevel = 25, maxLevel = 28 WHERE name = "Skinnymajinx" AND zoneid = 192;
UPDATE mob_groups SET minLevel = 12, maxLevel = 15 WHERE name = "Troika_Bats" AND zoneid = 192;

-- King Ranperres Tomb
UPDATE mob_groups SET minLevel = 62, maxLevel = 64 WHERE name = "Ogre_Bat" AND zoneid = 190;
UPDATE mob_groups SET minLevel = 58, maxLevel = 60 WHERE name = "Ossuary_Worm" AND zoneid = 190;
UPDATE mob_groups SET minLevel = 64, maxLevel = 66 WHERE name = "Bonnet_Beetle" AND zoneid = 190;
UPDATE mob_groups SET minLevel = 63, maxLevel = 65 WHERE name = "Barrow_Scorpion" AND zoneid = 190;

-- Korroloka Tunnel
UPDATE mob_groups SET minLevel = 28, maxLevel = 31 WHERE name = "Spool_Leech" AND zoneid = 173;
UPDATE mob_groups SET minLevel = 29, maxLevel = 32 WHERE name = "Lacerator" AND zoneid = 173;

-- Lufaise Meadows
UPDATE mob_groups SET minLevel = 41, maxLevel = 43 WHERE name = "Fomor_Bard" AND zoneid = 24;
UPDATE mob_groups SET minLevel = 41, maxLevel = 43 WHERE name = "Fomor_Beastmaster" AND zoneid = 24;
UPDATE mob_groups SET minLevel = 52, maxLevel = 54 WHERE name = "Fomor_Black_Mage" AND zoneid = 24;
UPDATE mob_groups SET minLevel = 52, maxLevel = 54 WHERE name = "Fomor_Dark_Knight" AND zoneid = 24;
UPDATE mob_groups SET minLevel = 52, maxLevel = 54 WHERE name = "Fomor_Dragoon" AND zoneid = 24;
UPDATE mob_groups SET minLevel = 34, maxLevel = 36 WHERE name = "Fomor_s_Wyvern" AND zoneid = 24;
UPDATE mob_groups SET minLevel = 52, maxLevel = 54 WHERE name = "Fomor_Monk" AND zoneid = 24;
UPDATE mob_groups SET minLevel = 52, maxLevel = 54 WHERE name = "Fomor_Ninja" AND zoneid = 24;
UPDATE mob_groups SET minLevel = 52, maxLevel = 54 WHERE name = "Fomor_Paladin" AND zoneid = 24;
UPDATE mob_groups SET minLevel = 41, maxLevel = 43 WHERE name = "Fomor_Ranger" AND zoneid = 24;
UPDATE mob_groups SET minLevel = 41, maxLevel = 43 WHERE name = "Fomor_Red_Mage" AND zoneid = 24;
UPDATE mob_groups SET minLevel = 41, maxLevel = 43 WHERE name = "Fomor_Samurai" AND zoneid = 24;
UPDATE mob_groups SET minLevel = 53, maxLevel = 55 WHERE name = "Fomor_Samurai" AND zoneid = 24;
UPDATE mob_groups SET minLevel = 41, maxLevel = 43 WHERE name = "Fomor_Summoner" AND zoneid = 24;
UPDATE mob_groups SET minLevel = 34, maxLevel = 36 WHERE name = "Fomor_s_Elemental" AND zoneid = 24;
UPDATE mob_groups SET minLevel = 42, maxLevel = 44 WHERE name = "Fomor_Warrior" AND zoneid = 24;
UPDATE mob_groups SET minLevel = 80, maxLevel = 83 WHERE name = "Abraxas" AND zoneid = 24; -- Fixed levels
UPDATE mob_groups SET minLevel = 33, maxLevel = 36 WHERE name = "Acrophies" AND zoneid = 24; -- Fixed levels
UPDATE mob_groups SET minLevel = 43, maxLevel = 44 WHERE name = "Air_Elemental" AND zoneid = 24; -- Fixed levels
UPDATE mob_groups SET minLevel = 34, maxLevel = 37 WHERE name = "Bugard" AND zoneid = 24; -- Fixed levels
UPDATE mob_groups SET minLevel = 38, maxLevel = 40 WHERE name = "Cluster" AND zoneid = 24; -- Fixed levels
UPDATE mob_groups SET minLevel = 33, maxLevel = 36 WHERE name = "Crimson_Knight_Crab" AND zoneid = 24; -- Fixed levels
UPDATE mob_groups SET minLevel = 84, maxLevel = 86 WHERE name = "Dark_Elemental" AND zoneid = 24; -- Fixed levels
UPDATE mob_groups SET minLevel = 35, maxLevel = 38 WHERE name = "Gigas_Slinger" AND zoneid = 24; -- Fixed levels
UPDATE mob_groups SET minLevel = 35, maxLevel = 38 WHERE name = "Gigas_Wrestler" AND zoneid = 24; -- Fixed levels
UPDATE mob_groups SET minLevel = 49, maxLevel = 53 WHERE name = "Leshachikha" AND zoneid = 24; -- Fixed levels
UPDATE mob_groups SET minLevel = 31, maxLevel = 34 WHERE name = "Miner_Bee" AND zoneid = 24; -- Fixed levels
UPDATE mob_groups SET minLevel = 35, maxLevel = 38 WHERE name = "Orcish_Beastrider" AND zoneid = 24; -- Fixed levels
UPDATE mob_groups SET minLevel = 35, maxLevel = 38 WHERE name = "Orcish_Brawler" AND zoneid = 24; -- Fixed levels
UPDATE mob_groups SET minLevel = 35, maxLevel = 38 WHERE name = "Orcish_Impaler" AND zoneid = 24; -- Fixed levels
UPDATE mob_groups SET minLevel = 35, maxLevel = 38 WHERE name = "Orcish_Nightraider" AND zoneid = 24; -- Fixed levels
UPDATE mob_groups SET minLevel = 37, maxLevel = 40 WHERE name = "Orcish_Stonelauncher" AND zoneid = 24; -- Fixed levels
UPDATE mob_groups SET minLevel = 81, maxLevel = 83 WHERE name = "Tavnazian_Ram" AND zoneid = 24; -- Fixed levels
UPDATE mob_groups SET minLevel = 43, maxLevel = 44 WHERE name = "Thunder_Elemental" AND zoneid = 24 AND groupid = 25; -- Fixed levels
UPDATE mob_groups SET minLevel = 62, maxLevel = 63 WHERE name = "Thunder_Elemental" AND zoneid = 24 AND groupid = 78; -- Fixed levels

-- Maze of Shakhrami
UPDATE mob_groups SET minLevel = 26, maxLevel = 29 WHERE name = "Warren_Bat" AND zoneid = 198;
UPDATE mob_groups SET minLevel = 23, maxLevel = 26 WHERE name = "Chaser_Bats" AND zoneid = 198;
UPDATE mob_groups SET minLevel = 24, maxLevel = 28 WHERE name = "Bleeder_Leech" AND zoneid = 198;
UPDATE mob_groups SET minLevel = 29, maxLevel = 31 WHERE name = "Crypterpillar" AND zoneid = 198;

-- Ordelles Caves
UPDATE mob_groups SET minLevel = 23, maxLevel = 26 WHERE name = "Buds_Bunny" AND zoneid = 193;
UPDATE mob_groups SET minLevel = 29, maxLevel = 31 WHERE name = "Targe_Beetle" AND zoneid = 193;
UPDATE mob_groups SET minLevel = 27, maxLevel = 29 WHERE name = "Swagger_Spruce" AND zoneid = 193;
UPDATE mob_groups SET minLevel = 25, maxLevel = 27 WHERE name = "Bilis_Leech" AND zoneid = 193;

-- Outer Horutoto Ruins
UPDATE mob_groups SET minLevel = 15, maxLevel = 18 WHERE name = "Fetor_Bats" AND zoneid = 194;
UPDATE mob_groups SET minLevel = 20, maxLevel = 23 WHERE name = "Thorn_Bat" AND zoneid = 194;
UPDATE mob_groups SET minLevel = 23, maxLevel = 25 WHERE name = "Fuligo" AND zoneid = 194;

-- Ranguemont Pass
UPDATE mob_groups SET minLevel = 26, maxLevel = 30 WHERE name = "Goblin_Artificer" AND zoneid = 166;
UPDATE mob_groups SET minLevel = 26, maxLevel = 30 WHERE name = "Goblin_Hoodoo" AND zoneid = 166;
UPDATE mob_groups SET minLevel = 26, maxLevel = 30 WHERE name = "Goblin_Tanner" AND zoneid = 166;
UPDATE mob_groups SET minLevel = 26, maxLevel = 30 WHERE name = "Goblin_Chaser" AND zoneid = 166;
UPDATE mob_groups SET minLevel = 25, maxLevel = 28 WHERE name = "Bilesucker" AND zoneid = 166;
UPDATE mob_groups SET minLevel = 42, maxLevel = 44 WHERE name = "Hovering_Oculus" AND zoneid = 166;

-- The Eldieme Necropolis
UPDATE mob_groups SET minLevel = 53, maxLevel = 55 WHERE name = "Nekros_Hound" AND zoneid = 195;
UPDATE mob_groups SET minLevel = 60, maxLevel = 63 WHERE name = "Hellbound_Warrior" AND zoneid = 195;
UPDATE mob_groups SET minLevel = 60, maxLevel = 63 WHERE name = "Hellbound_Warlock" AND zoneid = 195;

-- Toraimarai Canal
UPDATE mob_groups SET minLevel = 58, maxLevel = 60 WHERE name = "Deviling_Bats" AND zoneid = 169;
UPDATE mob_groups SET minLevel = 60, maxLevel = 62 WHERE name = "Plunderer_Crab" AND zoneid = 169;
UPDATE mob_groups SET minLevel = 57, maxLevel = 59 WHERE name = "Blackwater_Pugil" AND zoneid = 169;
UPDATE mob_groups SET minLevel = 65, maxLevel = 67 WHERE name = "Starborer" AND zoneid = 169;
UPDATE mob_groups SET minLevel = 65, maxLevel = 67 WHERE name = "Sodden_Bones" AND zoneid = 169;
UPDATE mob_groups SET minLevel = 65, maxLevel = 67 WHERE name = "Drowned_Bones" AND zoneid = 169;
UPDATE mob_groups SET minLevel = 64, maxLevel = 67 WHERE name = "Flume_Toad" AND zoneid = 169;
UPDATE mob_groups SET minLevel = 64, maxLevel = 66 WHERE name = "Rapier_Scorpion" AND zoneid = 169;
UPDATE mob_groups SET minLevel = 63, maxLevel = 65 WHERE name = "Poroggo_Excavator" AND zoneid = 169;

-- West Ronfaure
UPDATE mob_groups SET minLevel = 3, maxLevel = 6 WHERE name = "River_Crab" AND zoneid = 100;

-- Zeruhn Mines
UPDATE mob_groups SET minLevel = 2, maxLevel = 4 WHERE name = "Colliery_Bat" AND zoneid = 172;
UPDATE mob_groups SET minLevel = 3, maxLevel = 5 WHERE name = "Soot_Crab" AND zoneid = 172;
UPDATE mob_groups SET minLevel = 2, maxLevel = 4 WHERE name = "Burrower_Worm" AND zoneid = 172;
UPDATE mob_groups SET minLevel = 3, maxLevel = 6 WHERE name = "Veindigger_Leech" AND zoneid = 172;

INSERT INTO mob_droplist VALUES(2638,0,0,1000,16447,150);  -- ADD *rusty_dagger* from *Wendigo* was *160*
DELETE FROM mob_droplist WHERE itemID = "4824" AND dropType != 2  AND dropType != 4  AND dropID = "2638"; -- *remove gravity from war wendigo

UPDATE mob_pools SET cmbDelay = 150, immunity = 1 WHERE name = "Zipacna"; -- Speeds up delay
UPDATE mob_pools SET immunity = 17 WHERE name = "Mother_Globe"; -- Immune to sleep and silence
UPDATE mob_pools SET immunity = 641 WHERE name = "Despot"; -- Immune to Slow, Elegy and Sleep
UPDATE mob_pools SET immunity = 17 WHERE name = "Ullikummi"; -- Immune to sleep and silence
UPDATE mob_pools SET immunity = 641 WHERE name = "Faust"; -- Immune to Slow, Elegy and Sleep
UPDATE mob_pools SET immunity = 17 WHERE name = "Olla_Grande"; -- Immune to sleep and silence
UPDATE mob_pools SET immunity = 17 WHERE name = "Olla_Media"; -- Immune to sleep and silence
UPDATE mob_pools SET immunity = 17 WHERE name = "Olla_Pequena"; -- Immune to sleep and silence
UPDATE mob_pools SET immunity = 17 WHERE name = "Steam_Cleaner"; -- Immune to sleep and silence
UPDATE mob_pools SET immunity = 1 WHERE name = "Brigandish_Blade"; -- Immune to sleep
UPDATE mob_groups SET spawntype = 128 WHERE name = "Faust"; -- Set spawntype
UPDATE mob_groups SET spawntype = 128 WHERE name = "Zipacna"; -- Set spawntype
UPDATE mob_groups SET spawntype = 128 WHERE name = "Mother_Globe"; -- Set spawntype

UPDATE mob_groups SET respawntime = "720"  WHERE zoneid = 151 AND name = "Cutter"; -- respawntime was 792
UPDATE mob_groups SET respawntime = "720"  WHERE zoneid = 151 AND name = "Yagudo_Drummer_present"; -- respawntime was 792
UPDATE mob_groups SET respawntime = "720"  WHERE zoneid = 151 AND name = "Meat_Maggot"; -- respawntime was 792
UPDATE mob_groups SET respawntime = "720"  WHERE zoneid = 151 AND name = "Yagudo_Interrogator_pres"; -- respawntime was 792
UPDATE mob_groups SET respawntime = "720"  WHERE zoneid = 151 AND name = "Yagudo_Oracle"; -- respawntime was 792
UPDATE mob_groups SET respawntime = "720"  WHERE zoneid = 151 AND name = "Yagudo_Herald_present"; -- respawntime was 792
UPDATE mob_groups SET respawntime = "720"  WHERE zoneid = 151 AND name = "Yagudo_Priest_present"; -- respawntime was 792
UPDATE mob_groups SET respawntime = "720"  WHERE zoneid = 151 AND name = "Yagudo_Theologist_both"; -- respawntime was 792
UPDATE mob_groups SET respawntime = "720"  WHERE zoneid = 151 AND name = "Bastion_Bats"; -- respawntime was 792
UPDATE mob_groups SET respawntime = "720"  WHERE zoneid = 151 AND name = "Yagudo_Votary_present"; -- respawntime was 792

UPDATE mob_groups SET respawntime = "720"  WHERE zoneid = 149 AND name = "Davoi_Mush"; -- respawntime was 792
UPDATE mob_groups SET respawntime = "720"  WHERE zoneid = 149 AND name = "Davoi_Pugil"; -- respawntime was 792
UPDATE mob_groups SET respawntime = "720"  WHERE zoneid = 149 AND name = "Davoi_Wasp"; -- respawntime was 792
UPDATE mob_groups SET respawntime = "720"  WHERE zoneid = 149 AND name = "Wolf_Bat"; -- respawntime was 792
UPDATE mob_groups SET respawntime = "720"  WHERE zoneid = 149 AND name = "Orcish_Nightraider"; -- respawntime was 792
UPDATE mob_groups SET respawntime = "720"  WHERE zoneid = 149 AND name = "Orcish_Beastrider"; -- respawntime was 792
UPDATE mob_groups SET respawntime = "720"  WHERE zoneid = 149 AND name = "Orcish_Impaler"; -- respawntime was 792
UPDATE mob_groups SET respawntime = "720"  WHERE zoneid = 149 AND name = "Orcish_Fighter"; -- respawntime was 792
UPDATE mob_groups SET respawntime = "720"  WHERE zoneid = 149 AND name = "Orcish_Cursemaker"; -- respawntime was 792
UPDATE mob_groups SET respawntime = "720"  WHERE zoneid = 149 AND name = "Davoi_Hornet"; -- respawntime was 792
UPDATE mob_groups SET respawntime = "720"  WHERE zoneid = 149 AND name = "Orcish_Serjeant"; -- respawntime was 792
UPDATE mob_groups SET respawntime = "720"  WHERE zoneid = 149 AND name = "Wood_Bats"; -- respawntime was 792

UPDATE mob_groups SET spawntype = 8 WHERE name = "Cluster" and zoneid = 24; --  Clusters in Lufaise Meadows should only spawn during foggy weather
UPDATE mob_groups SET spawntype = 8 WHERE name = "Atomic_Cluster" and zoneid = 24; --  Atomic Clusters in Lufaise Meadows should only spawn during foggy weather
UPDATE mob_groups SET spawntype = 8 WHERE name = "Atomic_Cluster" and zoneid = 25; --  Atomic Clusters in Misareaux Coast should only spawn during foggy weather

DELETE FROM mob_spawn_mods WHERE mobid = "16814361" AND modid = 55; -- Remove Nunyunuwi 5 minutes idle despawn as is not era (https://ffxiclopedia.fandom.com/wiki/Nunyunuwi)
DELETE FROM mob_spawn_mods WHERE mobid = "17031401" AND modid = 55; -- Remove Big Bomb 5 minutes idle despawn as is not era

UPDATE mob_droplist SET itemRate = "500" WHERE itemID = "19210" AND dropType != 2  AND dropType != 4  AND dropID  = "2001"; -- *pinch_of_stygian_ash* from *Pixie* was *640*
UPDATE mob_droplist SET itemRate = "100" WHERE itemID = "953" AND dropType != 2  AND dropType != 4  AND dropID  = "2604"; -- *treant_bulb* from *Walking_Sapling* was *200*
UPDATE mob_droplist SET itemRate = "150" WHERE itemID = "637" AND dropType != 2  AND dropType != 4  AND dropID  = "1407"; -- *vial_of_slime_oil* from *Jelly* was *140*
UPDATE mob_droplist SET itemRate = "240" WHERE itemID = "17947" AND dropType != 2  AND dropType != 4  AND dropID  = "1664"; -- *garde_pick* from *Metallic_Slime* was *440*
UPDATE mob_droplist SET itemRate = "50" WHERE itemID = "645" AND dropType != 2  AND dropType != 4  AND dropID  = "1739"; -- *chunk_of_darksteel_ore* from *Morion_Worm* was *160*
UPDATE mob_droplist SET itemRate = "240" WHERE itemID = "852" AND dropType != 2  AND dropType != 4  AND dropID  = "1474"; -- *lizard_skin* from *Labyrinth_Lizard* was *970*
UPDATE mob_droplist SET itemRate = "150" WHERE itemID = "825" AND dropType != 2  AND dropType != 4  AND dropID  = "264"; -- *square_of_cotton_cloth* from *Bhuta* was *380*
UPDATE mob_droplist SET itemRate = "10" WHERE itemID = "508" AND dropType != 2  AND dropType != 4  AND dropID  = "1030"; -- *goblin_helm* from *Goblin_Bounty_Hunter* was *10*
UPDATE mob_droplist SET itemRate = "50" WHERE itemID = "841" AND dropType != 2  AND dropType != 4  AND dropID  = "2775"; -- *yagudo_feather* from *Yagudo_Votary_present* was *50*
UPDATE mob_droplist SET itemRate = "10" WHERE itemID = "507" AND dropType != 2  AND dropType != 4  AND dropID  = "1030"; -- *goblin_mail* from *Goblin_Bounty_Hunter* was *50*
UPDATE mob_droplist SET itemRate = "100" WHERE itemID = "841" AND dropType != 2  AND dropType != 4  AND dropID  = "2731"; -- *yagudo_feather* from *Yagudo_Persecutor* was *20*
UPDATE mob_droplist SET itemRate = "500" WHERE itemID = "880" AND dropType != 2  AND dropType != 4  AND dropID  = "1773"; -- *bone_chip* from *Namtar* was *1000*
UPDATE mob_droplist SET itemRate = "100" WHERE itemID = "4432" AND dropType != 2  AND dropType != 4  AND dropID  = "1700"; -- *kazham_pineapple* from *Mischievous_Micholas* was *40*
UPDATE mob_droplist SET itemRate = "50" WHERE itemID = "816" AND dropType != 2  AND dropType != 4  AND dropID  = "2131"; -- *spool_of_silk_thread* from *Rumble_Crawler* was *90*
UPDATE mob_droplist SET itemRate = "100" WHERE itemID = "839" AND dropType != 2  AND dropType != 4  AND dropID  = "2131"; -- *piece_of_crawler_cocoon* from *Rumble_Crawler* was *150*
UPDATE mob_droplist SET itemRate = "10" WHERE itemID = "508" AND dropType != 2  AND dropType != 4  AND dropID  = "1031"; -- *goblin_helm* from *Goblin_Bounty_Hunter* was *10*
UPDATE mob_droplist SET itemRate = "10" WHERE itemID = "940" AND dropType != 2  AND dropType != 4  AND dropID  = "872"; -- *revival_tree_root* from *Fomor_Ranger* was *30*
UPDATE mob_droplist SET itemRate = "100" WHERE itemID = "15386" AND dropType != 2  AND dropType != 4  AND dropID  = "872"; -- *pisces_subligar* from *Fomor_Ranger* was *80*
UPDATE mob_droplist SET itemRate = "150" WHERE itemID = "1690" AND dropType != 2  AND dropType != 4  AND dropID  = "488"; -- *hippogryph_tailfeather* from *Cloud_Hippogryph* was *560*
UPDATE mob_droplist SET itemRate = "10" WHERE itemID = "506" AND dropType != 2  AND dropType != 4  AND dropID  = "1071"; -- *square_of_coeurl_leather* from *Goblin_Furrier* was *20*
UPDATE mob_droplist SET itemRate = "10" WHERE itemID = "1537" AND dropType != 2  AND dropType != 4  AND dropID  = "617"; -- *behemoth_leather_missive* from *Demon_Knight* was *10*
UPDATE mob_droplist SET itemRate = "50" WHERE itemID = "841" AND dropType != 2  AND dropType != 4  AND dropID  = "2765"; -- *yagudo_feather* from *Yagudo_Zealot_present* was *50*
UPDATE mob_droplist SET itemRate = "100" WHERE itemID = "4375" AND dropType != 2  AND dropType != 4  AND dropID  = "1759"; -- *danceshroom* from *Mycohopper* was *40*
UPDATE mob_droplist SET itemRate = "50" WHERE itemID = "739" AND dropType != 2  AND dropType != 4  AND dropID  = "1961"; -- *chunk_of_orichalcum_ore* from *Oupire* was *100*
UPDATE mob_droplist SET itemRate = "150" WHERE itemID = "16341" AND dropType != 2  AND dropType != 4  AND dropID  = "801"; -- *aurum_cuisses* from *Experimental_Lamia* was *390*
UPDATE mob_droplist SET itemRate = "50" WHERE itemID = "1783" AND dropType != 2  AND dropType != 4  AND dropID  = "2514"; -- *sample_of_luminian_tissue* from *Ul_aern_whm* was *60*
UPDATE mob_droplist SET itemRate = "100" WHERE itemID = "1900" AND dropType != 2  AND dropType != 4  AND dropID  = "2514"; -- *high-quality_aern_organ* from *Ul_aern_whm* was *90*
DELETE from mob_droplist WHERE itemID = "2810" AND dropType != 2  AND dropType != 4  AND dropID  = "1961";
INSERT INTO `mob_droplist` VALUES(1961, 0, 0, 1000, 2810, 1000); -- *vial_of_ebur_pigment* from *Oupire* was *1000*
INSERT INTO `mob_droplist` VALUES(1961, 0, 0, 1000, 2810, 500); -- *vial_of_ebur_pigment* from *Oupire* was *500*
DELETE from mob_droplist WHERE itemID = "15021" AND dropType != 2  AND dropType != 4  AND dropID  = "1576";
INSERT INTO `mob_droplist` VALUES(1576, 0, 0, 1000, 15021, 240); -- *aurum_gauntlets* from *Mahjlaef_the_Paintorn* was *470*
INSERT INTO `mob_droplist` VALUES(1576, 0, 0, 1000, 15021, 150); -- *aurum_gauntlets* from *Mahjlaef_the_Paintorn* was *230*
DELETE from mob_droplist WHERE itemID = "1984" AND dropType != 2  AND dropType != 4  AND dropID  = "1849";
INSERT INTO `mob_droplist` VALUES(1849, 0, 0, 1000, 1984, 100); -- *snapping_mole* from *Old_Quadav* was *100
INSERT INTO `mob_droplist` VALUES(1849, 0, 0, 1000, 1984, 50); -- *snapping_mole* from *Old_Quadav* was *50
DELETE from mob_droplist WHERE itemID = "2810" AND dropType != 2  AND dropType != 4  AND dropID  = "1961";
INSERT INTO `mob_droplist` VALUES(1961, 0, 0, 1000, 2810, 1000);
INSERT INTO `mob_droplist` VALUES(1961, 0, 0, 1000, 2810, 500);
DELETE from mob_droplist WHERE itemID = "4370" AND dropType != 2  AND dropType != 4  AND dropID  = "583";
INSERT INTO `mob_droplist` VALUES(583, 0, 0, 1000, 4370, 100); -- *pot_of_honey* from *Death_from_Above* was *110*
INSERT INTO `mob_droplist` VALUES(583, 0, 0, 1000, 4370, 50); -- *pot_of_honey* from *Death_from_Above* was *60*
DELETE from mob_droplist WHERE itemID = "4400" AND dropType != 2  AND dropType != 4  AND dropID  = "203";
INSERT INTO `mob_droplist` VALUES(203, 0, 0, 1000, 4400, 100); -- *slice_of_land_crab_meat* from *Aydeewa_Crab* was *100*
INSERT INTO `mob_droplist` VALUES(203, 0, 0, 1000, 4400, 50); -- *slice_of_land_crab_meat* from *Aydeewa_Crab* was *50*
DELETE from mob_droplist WHERE itemID = "4400" AND dropType != 2  AND dropType != 4  AND dropID  = "2176";
INSERT INTO `mob_droplist` VALUES(2176, 0, 0, 1000, 4400, 100); -- *slice_of_land_crab_meat* from *Scavenger_Crab* was *100*
INSERT INTO `mob_droplist` VALUES(2176, 0, 0, 1000, 4400, 50); -- *slice_of_land_crab_meat* from *Scavenger_Crab* was *50*
DELETE from mob_droplist WHERE itemID = "4400" AND dropType != 2  AND dropType != 4  AND dropID  = "2178";
INSERT INTO `mob_droplist` VALUES(2178, 0, 0, 1000, 4400, 100); -- *slice_of_land_crab_meat* from *Scavenger_Crab* was *100*
INSERT INTO `mob_droplist` VALUES(2178, 0, 0, 1000, 4400, 50); -- *slice_of_land_crab_meat* from *Scavenger_Crab* was *50*
DELETE from mob_droplist WHERE itemID = "4400" AND dropType != 2  AND dropType != 4  AND dropID  = "416";
INSERT INTO `mob_droplist` VALUES(416, 0, 0, 1000, 4400, 1000); -- *slice_of_land_crab_meat* from *Cargo_Crab_Colin* was *1000*
INSERT INTO `mob_droplist` VALUES(416, 0, 0, 1000, 4400, 240); -- *slice_of_land_crab_meat* from *Cargo_Crab_Colin* was *500*
DELETE from mob_droplist WHERE itemID = "5367" AND dropType != 2  AND dropType != 4  AND dropID  = "151";
INSERT INTO `mob_droplist` VALUES(151, 0, 0, 1000, 5367, 1000); -- *cumulus_cell* from *Archaic_Chariot* was *1000*
INSERT INTO `mob_droplist` VALUES(151, 0, 0, 1000, 5367, 500); -- *cumulus_cell* from *Archaic_Chariot* was *500*
DELETE from mob_droplist WHERE itemID = "5369" AND dropType != 2  AND dropType != 4  AND dropID  = "151";
INSERT INTO `mob_droplist` VALUES(151, 0, 0, 1000, 5369, 1000); -- *stratus_cell* from *Archaic_Chariot* was *1000*
INSERT INTO `mob_droplist` VALUES(151, 0, 0, 1000, 5369, 500); -- *stratus_cell* from *Archaic_Chariot* was *500*
DELETE from mob_droplist WHERE itemID = "5372" AND dropType != 2  AND dropType != 4  AND dropID  = "151";
INSERT INTO `mob_droplist` VALUES(151, 0, 0, 1000, 5372, 1000); -- *virga_cell* from *Archaic_Chariot* was *1000*
INSERT INTO `mob_droplist` VALUES(151, 0, 0, 1000, 5372, 500); -- *virga_cell* from *Archaic_Chariot* was *500*
DELETE from mob_droplist WHERE itemID = "5372" AND dropType != 2  AND dropType != 4  AND dropID  = "949";
INSERT INTO `mob_droplist` VALUES(949, 0, 0, 1000, 5372, 100); -- *virga_cell* from *Gespenst* was *100*
INSERT INTO `mob_droplist` VALUES(949, 0, 0, 1000, 5372, 50); -- *virga_cell* from *Gespenst* was *50*
DELETE from mob_droplist WHERE itemID = "881" AND dropType != 2  AND dropType != 4  AND dropID  = "2113";
INSERT INTO `mob_droplist` VALUES(2113, 0, 0, 1000, 881, 100); -- *crab_shell* from *Rock_Crab* was *130*
INSERT INTO `mob_droplist` VALUES(2113, 0, 0, 1000, 881, 10); -- *crab_shell* from *Rock_Crab* was *10*
DELETE from mob_droplist WHERE itemID = "881" AND dropType != 2  AND dropType != 4  AND dropID  = "416";
INSERT INTO `mob_droplist` VALUES(416, 0, 0, 1000, 881, 500); -- *crab_shell* from *Cargo_Crab_Colin* was *510*
INSERT INTO `mob_droplist` VALUES(416, 0, 0, 1000, 881, 240); -- *crab_shell* from *Cargo_Crab_Colin* was *260*
DELETE from mob_droplist WHERE itemID = "897" AND dropType != 2  AND dropType != 4  AND dropID  = "1001";
INSERT INTO `mob_droplist` VALUES(1001, 0, 0, 1000, 897, 500); -- *scorpion_claw* from *Girtab* was *1000*
INSERT INTO `mob_droplist` VALUES(1001, 0, 0, 1000, 897, 240); -- *scorpion_claw* from *Girtab* was *500*
DELETE from mob_droplist WHERE itemID = "897" AND dropType != 2  AND dropType != 4  AND dropID  = "531";
INSERT INTO `mob_droplist` VALUES(531, 0, 0, 1000, 897, 1000); -- *scorpion_claw* from *Crawler_Hunter* was *1000*
INSERT INTO `mob_droplist` VALUES(531, 0, 0, 1000, 897, 500); -- *scorpion_claw* from *Crawler_Hunter* was *500*
DELETE from mob_droplist WHERE itemID = "919" AND dropType != 2  AND dropType != 4  AND dropID  = "1428";
INSERT INTO `mob_droplist` VALUES(1428, 0, 0, 1000, 919, 1000); -- *clump_of_boyahda_moss* from *Keeper_of_Halidom* was *1000*
INSERT INTO `mob_droplist` VALUES(1428, 0, 0, 1000, 919, 500); -- *clump_of_boyahda_moss* from *Keeper_of_Halidom* was *500*

UPDATE mob_droplist SET itemRate = "50" WHERE itemID = "16631" AND dropType != 2  AND dropType != 4  AND dropID  = "563"; -- *kaiser_sword* from *Dancing_Weapon* was *40*
UPDATE mob_droplist SET itemRate = "150" WHERE itemID = "17753" AND dropType != 2  AND dropType != 4  AND dropID  = "14"; -- *organics* from *Achamoth* was *310*
UPDATE mob_droplist SET itemRate = "150" WHERE itemID = "17753" AND dropType != 2  AND dropType != 4  AND dropID  = "14"; -- *organics* from *Achamoth* was *150*
UPDATE mob_droplist SET itemRate = "150" WHERE itemID = "16342" AND dropType != 2  AND dropType != 4  AND dropID  = "1576"; -- *oracles_braconi* from *Mahjlaef_the_Paintorn* was *320*
UPDATE mob_droplist SET itemRate = "100" WHERE itemID = "936" AND dropType != 2  AND dropType != 4  AND dropID  = "2103"; -- *chunk_of_rock_salt* from *River_Crab* was *110*
UPDATE mob_droplist SET itemRate = "100" WHERE itemID = "1119" AND dropType != 2  AND dropType != 4  AND dropID  = "2432"; -- *tonberry_coat* from *Tonberry_Cutter* was *110*
UPDATE mob_droplist SET itemRate = "100" WHERE itemID = "4759" AND dropType != 2  AND dropType != 4  AND dropID  = "678"; -- *scroll_of_blizzard_iii* from *Doom_Mage* was *140*
UPDATE mob_droplist SET itemRate = "150" WHERE itemID = "953" AND dropType != 2  AND dropType != 4  AND dropID  = "2604"; -- *treant_bulb* from *Walking_Sapling* was *220*
UPDATE mob_droplist SET itemRate = "150" WHERE itemID = "825" AND dropType != 2  AND dropType != 4  AND dropID  = "798"; -- *square_of_cotton_cloth* from *Evil_Spirit* was *210*

DELETE from mob_droplist WHERE itemID = "859" AND dropType != 2  AND dropType != 4  AND dropID  = "2329";
INSERT INTO `mob_droplist` VALUES(2329, 0, 0, 1000, 859, 500); -- *ram_skin* from *Steelfleece_Baldarich* was *1000*
INSERT INTO `mob_droplist` VALUES(2329, 0, 0, 1000, 859, 500); -- *ram_skin* from *Steelfleece_Baldarich* was *500*
INSERT INTO `mob_droplist` VALUES(2329, 0, 0, 1000, 859, 240); -- *ram_skin* from *Steelfleece_Baldarich* was *330*
INSERT INTO `mob_droplist` VALUES(2329, 0, 0, 1000, 859, 240); -- *ram_skin* from *Steelfleece_Baldarich* was *250*
INSERT INTO `mob_droplist` VALUES(2329, 0, 0, 1000, 859, 150); -- *ram_skin* from *Steelfleece_Baldarich* was *130*
INSERT INTO `mob_droplist` VALUES(2329, 0, 0, 1000, 859, 150); -- *ram_skin* from *Steelfleece_Baldarich* was *110*
DELETE from mob_droplist WHERE itemID = "859" AND dropType != 2  AND dropType != 4  AND dropID  = "304";
INSERT INTO `mob_droplist` VALUES(304, 0, 0, 1000, 859, 500); -- *ram_skin* from *Bloodtear_Baldurf* was *250*
INSERT INTO `mob_droplist` VALUES(304, 0, 0, 1000, 859, 500); -- *ram_skin* from *Bloodtear_Baldurf* was *130*
INSERT INTO `mob_droplist` VALUES(304, 0, 0, 1000, 859, 240); -- *ram_skin* from *Bloodtear_Baldurf* was *80*
INSERT INTO `mob_droplist` VALUES(304, 0, 0, 1000, 859, 240); -- *ram_skin* from *Bloodtear_Baldurf* was *60*
INSERT INTO `mob_droplist` VALUES(304, 0, 0, 1000, 859, 150); -- *ram_skin* from *Bloodtear_Baldurf* was *50*
INSERT INTO `mob_droplist` VALUES(304, 0, 0, 1000, 859, 150); -- *ram_skin* from *Bloodtear_Baldurf* was *40*