-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- -----------
-- GENERAL  --
-- -----------

-- HOTFIX ASAP PRE-PATCH
UPDATE mob_pools SET cmbDelay ="140" WHERE name = "King_Arthro"; -- Increase KA Attack speed to match reference video https://www.youtube.com/watch?v=mhDcFbpuS0Y&t=287s ---- Added to root Mob_pools

-- Juu Duzu the Whirlwind - deleting wrong spawn location
DELETE FROM nm_spawn_points WHERE mobid = 17371300 and pos = 5;

UPDATE item_equipment SET MId = 265 WHERE itemId = 16544; -- ryl.arc._sword was using wrong item model

-- Upper Delkfutts Tower
INSERT IGNORE INTO `nm_spawn_points` VALUES (17424480, 0, -322.953, -175.424, 0.762); -- Alkyoneus
INSERT IGNORE INTO `nm_spawn_points` VALUES (17424480, 1, -309.209, -175.424, 5.638); -- Alkyoneus
INSERT IGNORE INTO `nm_spawn_points` VALUES (17424480, 2, -294.361, -176.014, -13.924); -- Alkyoneus
INSERT IGNORE INTO `nm_spawn_points` VALUES (17424480, 3, -263.253, -176.000, 17.688); -- Alkyoneus
INSERT IGNORE INTO `nm_spawn_points` VALUES (17424480, 4, -276.544, -175.424, 62.725); -- Alkyoneus
INSERT IGNORE INTO `nm_spawn_points` VALUES (17424480, 5, -299.602, -175.424, 31.651); -- Alkyoneus

INSERT IGNORE INTO `nm_spawn_points` VALUES (17424444, 0, -301.672, -159.424, 28.727); -- Pallas
INSERT IGNORE INTO `nm_spawn_points` VALUES (17424444, 1, -317.560, -159.952, 51.111); -- Pallas
INSERT IGNORE INTO `nm_spawn_points` VALUES (17424444, 2, -333.110, -160.019, 24.398); -- Pallas
INSERT IGNORE INTO `nm_spawn_points` VALUES (17424444, 3, -307.259, -160.000, 20.870); -- Pallas
INSERT IGNORE INTO `nm_spawn_points` VALUES (17424444, 4, -295.153, -159.424, -0.382); -- Pallas
INSERT IGNORE INTO `nm_spawn_points` VALUES (17424444, 5, -259.163, -160.000, 23.274); -- Pallas

-- Halvung
INSERT IGNORE INTO `nm_spawn_points` VALUES (17031401, 0, -236.129, 14.087, 287.822); -- Big Bomb
INSERT IGNORE INTO `nm_spawn_points` VALUES (17031401, 1, -239.087, 13.913, 308.361); -- Big Bomb
INSERT IGNORE INTO `nm_spawn_points` VALUES (17031401, 2, -196.793, 3.672, 296.014); -- Big Bomb
INSERT IGNORE INTO `nm_spawn_points` VALUES (17031401, 3, -177.828, -6.129, 282.548); -- Big Bomb

-- Promy Holla Zone Rework
UPDATE mob_spawn_points SET pos_x = 104.9824, pos_y = 0, pos_z = -238.7194, pos_rot = 53 WHERE mobid = "16842996";
UPDATE mob_spawn_points SET pos_x = 104.6884, pos_y = 0, pos_z = -236.7257, pos_rot = 99 WHERE mobid = "16843010";
UPDATE mob_spawn_points SET pos_x = -4.4, pos_y = 0, pos_z = 166.9161, pos_rot = 22 WHERE mobid = "16842762";
UPDATE mob_spawn_points SET pos_x = -6.8225, pos_y = 0, pos_z = 155.3888, pos_rot = 30 WHERE mobid = "16842761";
UPDATE mob_spawn_points SET pos_x = -31.1769, pos_y = 0, pos_z = 128.5388, pos_rot = 61 WHERE mobid = "16842763";
UPDATE mob_spawn_points SET pos_x = -207.4494, pos_y = 0, pos_z = -85.8353, pos_rot = 122 WHERE mobid = "16842829";
UPDATE mob_spawn_points SET pos_x = -214.4471, pos_y = 0, pos_z = -92.8047, pos_rot = 60 WHERE mobid = "16842811";
UPDATE mob_spawn_points SET pos_x = -219.357, pos_y = 0, pos_z = -72.8066, pos_rot = 217 WHERE mobid = "16842810";
UPDATE mob_spawn_points SET pos_x = 202.3691, pos_y = 0, pos_z = -180.1865, pos_rot = 56 WHERE mobid = "16843023";
UPDATE mob_spawn_points SET pos_x = 58.102, pos_y = 0, pos_z = 297.2291, pos_rot = 52 WHERE mobid = "16842921";
UPDATE mob_spawn_points SET pos_x = 168.0189, pos_y = 0, pos_z = 338.3727, pos_rot = 0 WHERE mobid = "16842935";
UPDATE mob_spawn_points SET pos_x = 161.086, pos_y = 0, pos_z = -207.6107, pos_rot = 241 WHERE mobid = "16843015";
UPDATE mob_spawn_points SET pos_x = 159.2483, pos_y = 0, pos_z = -195.7994, pos_rot = 202 WHERE mobid = "16843013";
UPDATE mob_spawn_points SET pos_x = 277.5939, pos_y = 0, pos_z = -150.8496, pos_rot = 102 WHERE mobid = "16843026";
UPDATE mob_spawn_points SET pos_x = 251.4929, pos_y = 0, pos_z = -82.5969, pos_rot = 125 WHERE mobid = "16843031";
UPDATE mob_spawn_points SET pos_x = 211.6537, pos_y = 0, pos_z = -50.2975, pos_rot = 100 WHERE mobid = "16843032";
UPDATE mob_spawn_points SET pos_x = 217.7825, pos_y = 0, pos_z = -40.2925, pos_rot = 80 WHERE mobid = "16843033";
UPDATE mob_spawn_points SET pos_x = 124.7702, pos_y = 0, pos_z = -41.2027, pos_rot = 30 WHERE mobid = "16843034";
UPDATE mob_spawn_points SET pos_x = 2.6838, pos_y = 0, pos_z = -185.5633, pos_rot = 119 WHERE mobid = "16843039";
UPDATE mob_spawn_points SET pos_x = -6.7434, pos_y = 0, pos_z = -174.3297, pos_rot = 213 WHERE mobid = "16842808";
UPDATE mob_spawn_points SET pos_x = -7.31, pos_y = 0, pos_z = -203.4486, pos_rot = 162 WHERE mobid = "16842807";
UPDATE mob_spawn_points SET pos_x = 88.0521, pos_y = 0, pos_z = -95.7068, pos_rot = 126 WHERE mobid = "16842998";

-- Promy Dem Zone Rework
UPDATE mob_spawn_points SET pos_x = -281.4977, pos_y = 0, pos_z = -147.937, pos_rot = 194 WHERE mobid = "16851007";
UPDATE mob_spawn_points SET pos_x = 28.5979, pos_y = 0, pos_z = -242.4267, pos_rot = 90 WHERE mobid = "16851111";
UPDATE mob_spawn_points SET pos_x = 38.9762, pos_y = 0, pos_z = -249.727, pos_rot = 30 WHERE mobid = "16851115";
UPDATE mob_spawn_points SET pos_x = -224.9102, pos_y = 0, pos_z = 298.0087, pos_rot = 53 WHERE mobid = "";
UPDATE mob_spawn_points SET pos_x = -281.4455, pos_y = 0, pos_z = -148.3793, pos_rot = 180 WHERE mobid = "16851021";
UPDATE mob_spawn_points SET pos_x = 65.9505, pos_y = 0, pos_z = 75.2799, pos_rot = 250 WHERE mobid = "16851234";
UPDATE mob_spawn_points SET pos_x = 72.0093, pos_y = 0, pos_z = 109.3414, pos_rot = 170 WHERE mobid = "16851237";
UPDATE mob_spawn_points SET pos_x = 105.1381, pos_y = 0, pos_z = 145.7895, pos_rot = 218 WHERE mobid = "16851238";
UPDATE mob_spawn_points SET pos_x = 92.5125, pos_y = 0, pos_z = 113.3645, pos_rot = 175 WHERE mobid = "16851241";
UPDATE mob_spawn_points SET pos_x = 158.7571, pos_y = 0, pos_z = -260.9823, pos_rot = 50 WHERE mobid = "16850965";
UPDATE mob_spawn_points SET pos_x = 152.3412, pos_y = 0, pos_z = -274.252, pos_rot = 105 WHERE mobid = "16850964";
UPDATE mob_spawn_points SET pos_x = -241.0292, pos_y = 0, pos_z = 321.1828, pos_rot = 214 WHERE mobid = "16851106";
UPDATE mob_spawn_points SET pos_x = -230.8023, pos_y = 0, pos_z = 329.0938, pos_rot = 180 WHERE mobid = "16851103";
UPDATE mob_spawn_points SET pos_x = -159.966, pos_y = 0, pos_z = 253.1593, pos_rot = 64 WHERE mobid = "16851120";
UPDATE mob_spawn_points SET pos_x = -135.457, pos_y = 0, pos_z = 255.6546, pos_rot = 60 WHERE mobid = "16851116";
UPDATE mob_spawn_points SET pos_x = -39.9547, pos_y = 0, pos_z = 197.9651, pos_rot = 180 WHERE mobid = "16851128";
UPDATE mob_spawn_points SET pos_x = 137.2684, pos_y = 0, pos_z = 144.2999, pos_rot = 13 WHERE mobid = "16851233";
UPDATE mob_spawn_points SET pos_x = 131.069, pos_y = 0, pos_z = 210.4403, pos_rot = 203 WHERE mobid = "16851182";

-- Promy Mea Zone Rework
UPDATE mob_spawn_points SET pos_x = -7.9353, pos_y = 0, pos_z = 205.5068, pos_rot = 173 WHERE mobid = "16859403";
UPDATE mob_spawn_points SET pos_x = 15.6043, pos_y = 0, pos_z = 211.0029, pos_rot = 239 WHERE mobid = "16859405";
UPDATE mob_spawn_points SET pos_x = 32.8088, pos_y = 0, pos_z = 233.1867, pos_rot = 60 WHERE mobid = "16859414";
UPDATE mob_spawn_points SET pos_x = 18.6954, pos_y = 0, pos_z = 268.584, pos_rot = 90 WHERE mobid = "16859432";
UPDATE mob_spawn_points SET pos_x = -264.5457, pos_y = 0, pos_z = 15.9821, pos_rot = 214 WHERE mobid = "16859239";
UPDATE mob_spawn_points SET pos_x = -56.3249, pos_y = 0, pos_z = 29.2449, pos_rot = 152 WHERE mobid = "16859245";
UPDATE mob_spawn_points SET pos_x = -39.6501, pos_y = 0, pos_z = 37.2909, pos_rot = 148 WHERE mobid = "16859235";
UPDATE mob_spawn_points SET pos_x = -66.8032, pos_y = 0, pos_z = 53.6121, pos_rot = 101 WHERE mobid = "16859237";
UPDATE mob_spawn_points SET pos_x = 51.262, pos_y = 0, pos_z = 225.4719, pos_rot = 113 WHERE mobid = "16859400";
UPDATE mob_spawn_points SET pos_x = 0.3369, pos_y = 0, pos_z = 205.426, pos_rot = 82 WHERE mobid = "16859440";
UPDATE mob_spawn_points SET pos_x = -11.6207, pos_y = 0, pos_z = 272.4629, pos_rot = 13 WHERE mobid = "16859406";
UPDATE mob_spawn_points SET pos_x = -5.8938, pos_y = 0, pos_z = 290.7134, pos_rot = 165 WHERE mobid = "16859439";
UPDATE mob_spawn_points SET pos_x = 28.1076, pos_y = 0, pos_z = 281.0533, pos_rot = 88 WHERE mobid = "16859424";
UPDATE mob_spawn_points SET pos_x = 63.3434, pos_y = 0, pos_z = 231.1371, pos_rot = 220 WHERE mobid = "16859418";

-- Removing OOE Mob
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0, pos_rot = 0 WHERE mobname = "Ketos"; -- 2009