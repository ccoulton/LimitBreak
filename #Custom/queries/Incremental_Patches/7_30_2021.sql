-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- -----------
-- GENERAL  --
-- -----------

-- Qultada


INSERT IGNORE INTO mob_skills VALUES (108,1,'hunters_roll',1,8.0,2000,0,1,0,0,0,0,0,0); -- Add hunters_roll, Dummy animation, acc
INSERT IGNORE INTO mob_skills VALUES (105,1,'chaos_roll',1,8.0,2000,0,1,0,0,0,0,0,0); -- Add chaos_roll, Dummy animation, attp
INSERT IGNORE INTO mob_skills VALUES (110,1,'ninja_roll',1,8.0,2000,0,1,0,0,0,0,0,0); -- Add ninja_roll, Dummy animation, eva

INSERT IGNORE INTO mob_skills VALUES (2009,125,'fire_shot',0,18.0,2000,0,4,0,0,0,0,0,0); -- Add fire_shot, Dummy animation
INSERT IGNORE INTO mob_skills VALUES (2010,126,'ice_shot',0,18.0,2000,0,4,0,0,0,0,0,0); -- Add ice_shot, Dummy animation
INSERT IGNORE INTO mob_skills VALUES (2011,127,'wind_shot',0,18.0,2000,0,4,0,0,0,0,0,0); -- Add wind_shot, Dummy animation
INSERT IGNORE INTO mob_skills VALUES (2012,128,'earth_shot',0,18.0,2000,0,4,0,0,0,0,0,0); -- Add earth_shot, Dummy animation
INSERT IGNORE INTO mob_skills VALUES (2013,129,'thunder_shot',0,18.0,2000,0,4,0,0,0,0,0,0); -- Add thunder_shot, Dummy animation
INSERT IGNORE INTO mob_skills VALUES (2014,130,'water_shot',0,18.0,2000,0,4,0,0,0,0,0,0); -- Add water_shot, Dummy animation
INSERT IGNORE INTO mob_skills VALUES (2015,131,'light_shot',0,18.0,2000,0,4,0,0,0,0,0,0); -- Add light_shot, Dummy animation
INSERT IGNORE INTO mob_skills VALUES (2016,132,'dark_shot',0,18.0,2000,0,4,0,0,0,0,0,0); -- Add dark_shot, Dummy animation


-- Giddeus
DELETE FROM nm_spawn_points WHERE mobid = 17371515 and pos = 12; -- Hoo Mjuu the Torrent - deleting potentially problematic spawn position. Mob might clip into trees/rocks
DELETE FROM nm_spawn_points WHERE mobid = 17371515 and pos = 16; -- Hoo Mjuu the Torrent - deleting potentially problematic spawn position. Mob might clip into trees/rocks
DELETE FROM nm_spawn_points WHERE mobid = 17371515 and pos = 25; -- Hoo Mjuu the Torrent - deleting potentially problematic spawn position. Mob might clip into trees/rocks
DELETE FROM nm_spawn_points WHERE mobid = 17371515 and pos = 47; -- Hoo Mjuu the Torrent - deleting potentially problematic spawn position. Mob might clip into trees/rocks


UPDATE item_basic SET basesell = 171 WHERE itemid = 2014; -- bird_blood, was 342 gil.


UPDATE mob_family_system SET HP = 90 WHERE familyid = 258; -- Adjusting Worm base HP value (was too high)
UPDATE mob_family_system SET HP = 90 WHERE familyid = 276; -- Adjusting BigWorm base HP value (was too high)