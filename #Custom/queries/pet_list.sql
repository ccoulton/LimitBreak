-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- -------------------------------------------
-- Reverting WINGS custom 75 cap on HQ jugs --
-- -------------------------------------------

UPDATE pet_list SET maxLevel = 55 WHERE name = "LullabyMelodia";
UPDATE pet_list SET maxLevel = 55 WHERE name = "KeenearedSteffi";
UPDATE pet_list SET maxLevel = 63 WHERE name = "FlowerpotBen";
UPDATE pet_list SET maxLevel = 63 WHERE name = "SaberSiravarde";
UPDATE pet_list SET maxLevel = 65 WHERE name = "ColdbloodComo";
UPDATE pet_list SET maxLevel = 65 WHERE name = "ShellbusterOrob";
UPDATE pet_list SET maxLevel = 75 WHERE name = "ChopsueyChucky";