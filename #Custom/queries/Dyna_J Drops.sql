-- --------------------------------
-- Dynamis-Jeuno SQL Drop Tables --
-- --------------------------------

-- Normal Beastmen Mobs
DELETE FROM mob_droplist WHERE dropid = "2543" AND droptype != 2 AND itemid != "1520" AND itemid != "1470"; -- Delete all non-group items except sparkiling stone and goblin grease
UPDATE mob_droplist SET itemrate = "50" WHERE dropid = "2543" AND itemid = "1520"; -- Change drop rate on goblin grease from 8% to 5%
UPDATE mob_droplist SET itemrate = "50" WHERE dropid = "2543" AND itemid = "1470"; -- Change drop rate on sparkling stone from 8% to 5%
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15102, 71); -- Adds WAR AF to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15103, 71); -- Adds MNK AF to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15119, 71); -- Adds WHM AF to AF Gear Drop Group	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15135, 71); -- Adds BLM X to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15121, 71); -- Adds RDM X to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15137, 71); -- Adds THF X to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15124, 71); -- Adds DRK X to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15141, 71); -- Adds BRD X to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15082, 71); -- Adds RNG X to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15143, 71); -- Adds SAM X to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15144, 71); -- Adds NIN X to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15115, 71); -- Adds DRG X to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15028, 71); -- Adds COR X to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 16352, 77); -- Adds PUP X to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(2543, 0, 0, 1000, 1455, 100); -- Adds 1 bill 
INSERT INTO mob_droplist VALUES(2543, 0, 0, 1000, 1449, 100); -- Adds 1 shell 
INSERT INTO mob_droplist VALUES(2543, 0, 0, 1000, 1452, 100); -- Adds 1 coin 
INSERT INTO mob_droplist VALUES(2543, 0, 0, 1000, 1456, 1); -- Adds 1 Hbill 
INSERT INTO mob_droplist VALUES(2543, 0, 0, 1000, 1450, 1); -- Adds 1 Hshell 
INSERT INTO mob_droplist VALUES(2543, 0, 0, 1000, 1453, 1); -- Adds 1 Hcoin 

-- NM Beastmen Mobs
DELETE FROM mob_droplist WHERE dropid = "143" AND droptype != 2 AND itemid != "1520" AND itemid != "1470"; -- Delete all non-group items except sparkiling stone and goblin grease
UPDATE mob_droplist SET itemrate = "100" WHERE dropid = "143" AND itemid = "1520"; -- Change drop rate on goblin grease from 8% to 5%
UPDATE mob_droplist SET itemrate = "100" WHERE dropid = "143" AND itemid = "1470"; -- Change drop rate on sparkling stone from 8% to 5%
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15102, 71); -- Adds WAR AF to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15103, 71); -- Adds MNK AF to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15119, 71); -- Adds WHM AF to AF Gear Drop Group	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15135, 71); -- Adds BLM X to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15121, 71); -- Adds RDM X to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15137, 71); -- Adds THF X to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15124, 71); -- Adds DRK X to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15141, 71); -- Adds BRD X to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15082, 71); -- Adds RNG X to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15143, 71); -- Adds SAM X to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15144, 71); -- Adds NIN X to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15115, 71); -- Adds DRG X to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15028, 71); -- Adds COR X to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 16352, 77); -- Adds PUP X to AF Gear Drop Group 	[ 71 x 13, 77 x 1 == 1000]
INSERT INTO mob_droplist VALUES(143, 0, 0, 1000, 1455, 100); -- Adds 1 bill 
INSERT INTO mob_droplist VALUES(143, 0, 0, 1000, 1449, 100); -- Adds 1 shell 
INSERT INTO mob_droplist VALUES(143, 0, 0, 1000, 1452, 100); -- Adds 1 coin 
INSERT INTO mob_droplist VALUES(143, 0, 0, 1000, 1455, 100); -- Adds 1 bill 
INSERT INTO mob_droplist VALUES(143, 0, 0, 1000, 1449, 100); -- Adds 1 shell 
INSERT INTO mob_droplist VALUES(143, 0, 0, 1000, 1452, 100); -- Adds 1 coin 
INSERT INTO mob_droplist VALUES(143, 0, 0, 1000, 1456, 10); -- Adds 1 Hbill 
INSERT INTO mob_droplist VALUES(143, 0, 0, 1000, 1450, 10); -- Adds 1 Hshell 
INSERT INTO mob_droplist VALUES(143, 0, 0, 1000, 1453, 10); -- Adds 1 Hcoin 

-- Replicas
UPDATE mob_droplist SET itemrate = "100" WHERE dropid = "1144" AND itemid = " 749"; -- Change myth coin from 5% to 10%
UPDATE mob_droplist SET itemrate = "50" WHERE dropid = "1144" AND itemid = " 748"; -- Change gold coin 2% to 5%
UPDATE mob_droplist SET droptype = "1", groupid = "1", grouprate = "50", itemrate = "333" WHERE dropid = "1144" AND itemid = "1455"; -- Change 1 bill to group, change group rate, change itemrate
UPDATE mob_droplist SET droptype = "1", groupid = "1", grouprate = "50", itemrate = "333" WHERE dropid = "1144" AND itemid = "1449"; -- Change 1 shell to group, change group rate, change itemrate
UPDATE mob_droplist SET droptype = "1", groupid = "1", grouprate = "50", itemrate = "334" WHERE dropid = "1144" AND itemid = "1452"; -- Change 1 shell to group, change group rate, change itemrate

-- Megaboss
DELETE FROM mob_droplist WHERE dropid = "1085" AND itemid != "749" AND itemid != "748" AND itemid != "1474"; -- Delete fiendish tome, OOE
UPDATE mob_droplist SET itemrate = "240" WHERE dropid = "1085" AND itemid = "749"; -- change myth coin from 1% to 24%
UPDATE mob_droplist SET itemrate = "100" WHERE dropid = "1085" AND itemid = "748"; -- change myth coin from 1% to 10%
UPDATE mob_droplist SET itemrate = "150" WHERE dropid = "1085" AND itemid = "1474"; -- change infinity core from 1% to 10%
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 1455, 240); -- add single bill 
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 1449, 240); -- add single shell 
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 1455, 240); -- add single coin 
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 1456, 150); -- add  Hbill 
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 1450, 150); -- add Hshell 
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 1453, 150); -- add Hcoin 


-- assign correct drop pools to each mob 
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Smithy";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Alchemist";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Ambusher";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Armorer";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Dragontamer";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Enchanter";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Hitman";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Maestro";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Necromancer";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Pathfinder";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Pitfighter";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Ronin";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Shaman";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Tinkerer";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Welldigger";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Anvilix_Sootwrists";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Bandrix_Rockjaw";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Blazox_Boneybod";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Bootrix_Jaggedelbow";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Buffrix_Eargone";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Cloktix_Longnail";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Distilix_Stickytoes";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Elixmix_Hooknose";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Eremix_Snottynostril";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Feralox_Honeylips";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Feraloxs_Slime";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Gabblox_Magpietongue";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Hermitrix_Toothrot";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Humnox_Drumbelly";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Jabbrox_Grannyguise";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Jabkix_Pigeonpecs";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Karashix_Swollenskull";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Kikklix_Longlegs";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Lurklox_Dhalmelneck";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Mobpix_Mucousmouth";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Morgmox_Moldnoggin";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Mortilox_Wartpaws";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Prowlox_Barrelbelly";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Quicktrix_Hexhands";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Rutrix_Hamgams";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Scourquix_Scaleskin";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Scourquixs_Wyvern";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Scruffix_Shaggychest";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Slystix_Megapeepers";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Smeltix_Thickhide";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Snypestix_Eaglebeak";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Sparkspox_Sweatbrow";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Ticktox_Beadyeyes";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Trailblix_Goatmug";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Tufflix_Loglimbs";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Tymexox_Ninefingers";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Wasabix_Callusdigit";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Wilywox_Tenderpalm";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Wyrmwix_Snakespecs";
UPDATE mob_groups SET dropid = "1085" WHERE zoneid = "188" AND name ="Goblin_Golem";
UPDATE mob_groups SET dropid = "1144" WHERE zoneid = "188" AND name ="Goblin_Replica";
UPDATE mob_groups SET dropid = "1144" WHERE zoneid = "188" AND name ="Goblin_Replica";
UPDATE mob_groups SET dropid = "1144" WHERE zoneid = "188" AND name ="Goblin_Statue";
UPDATE mob_groups SET dropid = "1144" WHERE zoneid = "188" AND name ="Goblin_Statue";
UPDATE mob_groups SET dropid = "1144" WHERE zoneid = "188" AND name ="Goblin_Statue";
UPDATE mob_groups SET dropid = "1144" WHERE zoneid = "188" AND name ="Goblin_Statue";

-- clear respawn time and set all mobs to scripted spawn only
UPDATE mob_groups SET respawntime = "0", spawntype = "128" WHERE zoneid = "188";

-- corrects all groups levels for era
UPDATE mob_groups SET minlevel = "75", maxlevel = "77" WHERE zoneid = "188" AND minlevel != "85";

