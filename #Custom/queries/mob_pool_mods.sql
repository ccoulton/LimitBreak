-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------

-- -----------
-- GENERAL  --
-- -----------

-- CoP 5-3U Where Messengers Gather (Boneyard Gully Fight)
 -- Shikaree_X
DELETE FROM mob_pool_mods WHERE poolid = 3598;
INSERT IGNORE INTO mob_pool_mods VALUE(3598,44,1,1); -- Gives dual wield ability
INSERT IGNORE INTO mob_pool_mods VALUE(3598,3,1,1); -- Add MP base to Shikaree_X

 -- Shikaree_Y
DELETE FROM mob_pool_mods WHERE poolid = 3600;
INSERT IGNORE INTO mob_pool_mods VALUE(3600,291,15,0); -- Gives 15% counter rate

 -- Shikaree_Z
DELETE from mob_pool_mods where poolid = 3601; -- Regain 70/tick was defined here. This was double dipping and is not needed for Shikaree_Z as regain is defined in the lua.

 -- CoP 3-3 Diabolos
DELETE FROM mob_pool_mods WHERE poolid = 1027; -- Removed draw-in from DB and adde it to lua as per comment in mob_modifer "// 1 - player draw in, 2 - alliance draw in -- only add as a spawn mod!"

 -- Sabotender Bailaor
INSERT IGNORE INTO `mob_pool_mods` VALUES(6187, 1, 3000, 1); -- Min gil
INSERT IGNORE INTO `mob_pool_mods` VALUES(6187, 2, 13640, 1); -- Max gil

-- ---------
-- OTHERS --
-- ---------


