-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- These will update the default values for NEW characters. Any characters that already have slots --
-- will be unaffected. THIS MUST BE DONE BEFORE ANY CHARACTERS ARE CREATED!!!!!!                   --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- ----------
-- General --
-- ----------

UPDATE fishing_fish SET min_skill_level = "57", skill_level = "64" WHERE NAME = "Mythril Sword"; -- Was 0 and 7 before
UPDATE fishing_fish SET min_skill_level = "57", skill_level = "64" WHERE NAME = "Mythril Dagger"; -- Was 0 and 7 before
