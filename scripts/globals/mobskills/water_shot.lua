-----------------------------------
-- Ability: Water Shot
-- Consumes a Water Card to enhance water-based debuffs. Deals water-based magic damage
-- Drown Effect
-----------------------------------
require("scripts/globals/ability")
require("scripts/globals/magic")
require("scripts/globals/status")
require("scripts/globals/monstertpmoves")
-----------------------------------

function onMobSkillCheck(target, mob, skill)
    return 0
end

function onMobWeaponSkill(target, mob, skill)
	local dmgmod = 1
	local info = MobMagicalMove(mob, target, skill, mob:getWeaponDmg()*2, tpz.magic.ele.WATER, dmgmod, TP_MAB_BONUS, 1)
    local dmg = MobFinalAdjustments(info.dmg, mob, skill, target, tpz.attackType.MAGICAL, tpz.damageType.WATER)

	target:takeDamage(dmg, mob, tpz.attackType.MAGICAL, tpz.damageType.WATER)
	--[[ Remove additional status effect application as i misunderstand the ability.
    if dmg > 0 then
        local effects = {}
        local drown = target:getStatusEffect(tpz.effect.DROWN)
        if drown == nil then
            table.insert(effects, tpz.effect.DROWN)
        end

        local poison = target:getStatusEffect(tpz.effect.POISON)
        if poison == nil then
            table.insert(effects, tpz.effect.POISON)
        end

		if #effects > 0 then
		local roll = math.random(#effects)
		local applyeffect = effects[roll]
			if applyeffect == tpz.effect.DROWN then
				if target:getStatusEffect(tpz.effect.BURN) ~= nil then
					target:delStatusEffect(tpz.effect.BURN)
				end
			target:addStatusEffect(applyeffect, 3, 3, math.random(9,27))
			end
		end
    end
	]]
    return dmg
end
