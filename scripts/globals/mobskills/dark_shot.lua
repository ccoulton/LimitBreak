-----------------------------------
-- Ability: Dark Shot
-- Dispel 1 effect from target
-- Apply Bio or Blind
-----------------------------------
require("scripts/globals/ability")
require("scripts/globals/magic")
require("scripts/globals/status")
require("scripts/globals/monstertpmoves")
-----------------------------------

function onMobSkillCheck(target, mob, skill)
    return 0
end

function onMobWeaponSkill(target, mob, skill)
	-- Remove additional status effect application as i misunderstand the ability.
	--[[local effects = {}

	local bio = target:getStatusEffect(tpz.effect.BIO)
	if bio == nil then
		table.insert(effects, tpz.effect.BIO)
	end
	
	local blind = target:getStatusEffect(tpz.effect.BLINDNESS)
	if blind == nil then
		table.insert(effects, tpz.effect.BLINDNESS)
	end

	if #effects > 0 then
	local roll = math.random(#effects)
	local applyeffect = effects[roll]
		if applyeffect == tpz.effect.BIO then
			if target:getStatusEffect(tpz.effect.DIA) ~= nil then
				target:delStatusEffect(tpz.effect.DIA)
			end
		target:addStatusEffect(applyeffect, 3, 3, math.random(9,27))
		end
	end
	]]
    skill:setMsg(tpz.msg.basic.SKILL_ERASE)
    local dispelledEffect = target:dispelStatusEffect()
    if dispelledEffect == tpz.effect.NONE then
        -- no effect
       skill:setMsg(tpz.msg.basic.SKILL_NO_EFFECT)
    end



    return dispelEffect
end
