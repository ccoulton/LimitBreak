-- Area: Fei'Yin
-- Mob: Miser Murphy
-- Involved in Quest: Peace for the Spirit - RDM AF3
-----------------------------------
require("scripts/globals/status")
require("scripts/globals/mobs")
-----------------------------------

function onMobSpawn (mob)
    mob:setMobMod(tpz.mobMod.NO_STANDBACK, 1)
    mob:setMobMod(tpz.mobMod.ADD_EFFECT, 1)
    mob:setMod(tpz.mod.MACC, 80)
    mob:setMod(tpz.mod.UFASTCAST, 50)
    mob:setMod(tpz.mod.BINDRESTRAIT, 50)
    mob:setMod(tpz.mod.STUNRESTRAIT, 50)
    mob:setMod(tpz.mod.SILENCERESTRAIT, 95)
    mob:setMod(tpz.mod.RESBUILD_GRAVITY, 10)
    mob:setMod(tpz.mod.ACC, 250)
    mob:setMobMod(tpz.mobMod.MAGIC_COOL, 30)
    mob:addStatusEffectEx(tpz.effect.ICE_SPIKES, 0, 60, 0, 0)
end

function onAdditionalEffect(mob, target, damage)
    return tpz.mob.onAddEffect(mob, target, damage, tpz.mob.ae.HP_DRAIN, {chance = 50, power = math.random(450, 550)})
end