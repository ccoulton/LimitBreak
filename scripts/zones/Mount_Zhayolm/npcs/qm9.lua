-----------------------------------
-- Area: Mount Zhayolm
--  NPC: ??? (Cast Metal Plate) 
--	ID 17027568
-- !pos 152.6540 	-14.2256 	-146.7709
--		258.4493	-16.2094	-220.9087
--		389.5214	-14.3110	-228.9190
--		462.3387	-15.5414	-59.2175
--		600.1025	-17.8908	14.2162
--		732.7904	-22.9902	19.1166
--		617.6666	-24.0545	102.9118
--		570.8515	-25.2351	212.2957
--		789.2047	-21.8835	329.7796
-----------------------------------
local ID = require("scripts/zones/Mount_Zhayolm/IDs")
require("scripts/globals/npc_util")
-----------------------------------

function onTrade(player, npc, trade)
end

function onTrigger(player, npc)
	if player:getCharVar("HalvungWest") == 1 or player:getCharVar("HalvungEast") == 1 then
		player:addKeyItem(tpz.ki.CAST_METAL_PLATE)
		player:messageSpecial(ID.text.CAST_METAL_PLATE, tpz.ki.CAST_METAL_PLATE)
		player:setCharVar("HalvungWest", 0)
		player:setCharVar("HalvungEast", 0)
		
	else
		player:messageSpecial(ID.text.ORDINARY)
	end
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)
end
