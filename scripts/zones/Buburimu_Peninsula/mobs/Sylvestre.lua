-----------------------------------
-- Area: Buburimu Peninsula
--  Mob: Sylvestre
-----------------------------------
require("scripts/globals/regimes")
require("scripts/globals/status")
-----------------------------------
function onMobSpawn(mob)
	mob:addMod(tpz.mod.ATTP, -30)
end

function onMobDeath(mob, player, isKiller)
    tpz.regime.checkRegime(player, mob, 32, 1, tpz.regime.type.FIELDS)
    tpz.regime.checkRegime(player, mob, 33, 2, tpz.regime.type.FIELDS)
end
