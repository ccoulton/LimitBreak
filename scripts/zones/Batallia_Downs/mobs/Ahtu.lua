-----------------------------------
-- Area: Batallia Downs (105)
--  Mob: Ahtu
-----------------------------------

function onMobDeath(mob, player, isKiller)
end

function onMobDespawn(mob)
    UpdateNMSpawnPoint(mob:getID())
    local respawn = math.random(7200, 14400) -- 2 to 4 hours
    mob:setRespawnTime(respawn)
    SetServerVariable("AhtuRespawn",(os.time() + respawn))
end

