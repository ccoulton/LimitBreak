-----------------------------------
-- Area: Horlais Peak
--  Mob: Pilwiz
-- BCNM: Carapace Combatants
-----------------------------------
require("scripts/globals/status")
-----------------------------------
function onMobInitialize(mob)
    mob:addMod(tpz.mod.FASTCAST, 75)
end

function onMobSpawn(mob)
    mob:setMobMod(tpz.mobMod.NO_MOVE, 1)
    mob:setMobMod(tpz.mobMod.LINK_RADIUS, 30)
    mob:setMobMod(tpz.mobMod.SIGHT_RANGE, 15)
end

function onMobEngaged(mob, target)
    mob:setMobMod(tpz.mobMod.NO_MOVE, 0)
    mob:setLocalVar("StonegaII", os.time() + 8) -- First cast 8 seconds into the fight
end

function onMobFight(mob, target)
    if  os.time() > mob:getLocalVar("StonegaII") and mob:hasStatusEffect(tpz.effect.SILENCE) == false then
        mob:setLocalVar("StonegaII", os.time() + 15)
        mob:castSpell(190, target)
        mob:addMP(100)
    end
end

function onMobDeath(mob, player, isKiller)
end
