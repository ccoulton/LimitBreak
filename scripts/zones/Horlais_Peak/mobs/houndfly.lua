-----------------------------------
-- Area: Horlais Peak
-- Mob:  Houndfly
-- BCNM: Dropping Like Flies
-----------------------------------

function onMobSpawn(mob)
    mob:setMobMod(tpz.mobMod.CHARMABLE, 0)
    mob:setMod(tpz.mod.LULLABYRES, 100)
end