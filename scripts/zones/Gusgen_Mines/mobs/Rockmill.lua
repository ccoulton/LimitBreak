-----------------------------------
-- Area: Gusgen Mines
--  Mob: Rockmill
-----------------------------------
-- mixins = {require("scripts/mixins/families/worm")}
require("scripts/globals/regimes")
-----------------------------------

function onMobDeath(mob, player, isKiller)
    tpz.regime.checkRegime(player, mob, 685, 2, tpz.regime.type.GROUNDS)
end
