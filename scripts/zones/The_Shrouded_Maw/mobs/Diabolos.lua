-----------------------------------
-- Area: The Shrouded Maw
--  Mob: Diabolos
-----------------------------------
local ID = require("scripts/zones/The_Shrouded_Maw/IDs")

-- TODO: CoP Diabolos
-- 1) Make the diremites in the pit all aggro said player that falls into region. Should have a respawn time of 10 seconds.
-- 2) Diremites also shouldnt follow you back to the fight area if you make it there. Should despawn and respawn instantly if all players
--    make it back to the Diabolos floor area.

-- TODO: Diabolos Prime
-- Note: Diabolos Prime fight drops all tiles at once.

function closeAllTiles(mob)
    local inst = 1
    local tile = ID.npc.DARKNESS_NAMED_TILE_OFFSET + (inst - 1) * 8
    for i = tile, tile + 23 do
        local tilenpc = GetNPCByID(i)
        if tilenpc then
            tilenpc:setAnimation(tpz.anim.CLOSE_DOOR)
            tilenpc:setLocalVar("dropping", 0)
        end
    end
end

function onMobSpawn(mob)
    closeAllTiles(mob)
    mob:addMod(tpz.mod.DOUBLE_ATTACK, 8)
    mob:setMobMod(tpz.mobMod.DRAW_IN, 1)
    mob:setMobMod(tpz.mobMod.DRAW_IN_INCLUDE_PARTY, 1)
    -- Only add these for the CoP Diabolos NOT Prime
    local copD = mob:getID() - ID.mob.DIABOLOS_OFFSET
    if (copD >= 0 and copD <= 14) then
        mob:addMod(tpz.mod.ACC, 10)
        mob:addMod(tpz.mod.INT, -50)
        mob:addMod(tpz.mod.MND, -50)
        mob:addMod(tpz.mod.ATTP, -40)
        mob:addMod(tpz.mod.DEFP, -15)
        mob:addMod(tpz.mod.MDEF, -40)

    end
    local random = math.random(50,75)
    mob:setLocalVar("random_hp", random)
end

function onMobFight(mob, target)
    local mobOffset = mob:getID() - ID.mob.DIABOLOS_OFFSET
    if (mobOffset >= 0 and mobOffset <= 14) then
        local inst = math.floor(mobOffset/7)
        local hp = mob:getLocalVar("random_hp")
        local tileDrops =
        {
            {hp, "byc1", "bya1", "byb1"},
            {hp, "byc2", "bya2", "byb2"},
            {hp, "byc3", "bya3", "byb3"},
            {hp, "byc4", "bya4", "byb4"},
            {hp, "byc5", "bya5", "byb5"},
            {hp, "byc6", "bya6", "byb6"},
            {hp, "byc7", "bya7", "byb7"},
            {hp, "byc8", "bya8", "byb8"},
        }

        local hpp = ((mob:getHP()/mob:getMaxHP())*100)
        for k, v in pairs(tileDrops) do
            if (hpp < v[1]) then
                local tileId = ID.npc.DARKNESS_NAMED_TILE_OFFSET + (inst * 8) + (k - 1)
                local tilenpc = GetNPCByID(tileId)
                if (tilenpc:getAnimation() == tpz.anim.CLOSE_DOOR and tilenpc:getLocalVar("dropping") == 0) then
                    SendEntityVisualPacket(tileId, v[inst+2])  -- Animation for floor dropping
                    SendEntityVisualPacket(tileId, "s123")     -- Tile dropping sound
                    tilenpc:setLocalVar("dropping", 1)
                    tilenpc:timer(5000, function(tile)
                        tile:setAnimation(dsp.anim.OPEN_DOOR);
                    end)
                end
            end
        end
    end
end

function onMobDeath(mob, player, isKiller)
end
