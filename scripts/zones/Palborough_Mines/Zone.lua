-----------------------------------
--
-- Zone: Palborough Mines (143)
--
-----------------------------------
local ID = require("scripts/zones/Palborough_Mines/IDs")
require("scripts/globals/conquest")
require("scripts/globals/treasure")
require("scripts/globals/helm")
require("scripts/globals/zone")
-----------------------------------

function onInitialize(zone)
    tpz.treasure.initZone(zone)
    tpz.helm.initZone(zone, tpz.helm.type.MINING)

    local nomhore = GetServerVariable("NoMhoCrimsonarmorRespawn")
    if os.time() < nomhore then
        GetMobByID(ID.mob.NO_MHO_CRIMSONARMOR):setRespawnTime(nomhore - os.time())
    else
        GetMobByID(ID.mob.NO_MHO_CRIMSONARMOR):setRespawnTime(300)
    end
end

function onZoneIn(player, prevZone)
    local cs = -1

    if player:getXPos() == 0 and player:getYPos() == 0 and player:getZPos() == 0 then
        player:setPos(114.483, -42, -140, 96)
    end

    return cs
end

function onConquestUpdate(zone, updatetype)
    tpz.conq.onConquestUpdate(zone, updatetype)
end

function onRegionEnter(player, region)
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)
end
