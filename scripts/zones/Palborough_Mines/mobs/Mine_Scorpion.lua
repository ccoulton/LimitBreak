-----------------------------------
-- Area: Palborough Mines
--  Mob: Mine Scorpion
-----------------------------------
local ID = require("scripts/zones/Palborough_Mines/IDs")
require("scripts/globals/mobs")
-----------------------------------

function onMobDeath(mob, player, isKiller)
end

function onMobDespawn(mob)
    tpz.mob.phOnDespawn(mob, ID.mob.SCIMITAR_SCORPION_PH, 50, 1) -- 1 hour
end
