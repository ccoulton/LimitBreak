-----------------------------------
-- Area: Bibiki Bay
--  Mob: Shen
-----------------------------------
mixins = {require("scripts/mixins/families/uragnite")}
require("scripts/globals/settings")
require("scripts/globals/status")
-----------------------------------

function onMobFight(mob, target)
	if (mob:getBattleTime() > 0 and mob:getBattleTime() < 60) then
	tpz.mix.uragnite.config(mob, {chanceToShell = 0})
    elseif (mob:getBattleTime() > 60 and mob:getBattleTime() < 100) then
		local mobId = mob:getID()
			for i = 1, 2 do
            if (not GetMobByID(mobId+i):isSpawned()) then
			   SpawnMob(mobId+i):updateEnmity(target)
			   tpz.mix.uragnite.config(mob, {chanceToShell = 100, inShellRegen = 1000, timeInShellMin = 30, timeInShellMax = 40,})
			end
		end	
	elseif (mob:getBattleTime() > 100) then
	tpz.mix.uragnite.config(mob, {chanceToShell = 20, inShellRegen = 500, timeInShellMin = 20, timeInShellMax = 30,})
        local mobId = mob:getID()
        for i = 1, 2 do
            if (not GetMobByID(mobId+i):isSpawned()) then
                SpawnMob(mobId+i):updateEnmity(target)
            end
        end
    end
end

function onMobDeath(mob, player, isKiller)
end

function onMonsterMagicPrepare(mob, target)
    -- casts Water IV, Waterga III, Flood, Drown
    local rnd = math.random()

    if (rnd < 0.5) then
        return 201 -- waterga 3
    elseif (rnd < 0.7) then
        return 172 -- water 4
    elseif (rnd < 0.9) then
        return 214 -- flood
    else
        return 240 -- drown
    end

end
