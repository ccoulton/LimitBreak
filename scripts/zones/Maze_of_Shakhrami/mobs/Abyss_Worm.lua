-----------------------------------
-- Area: Maze of Shakhrami
--  Mob: Abyss Worm
-----------------------------------
-- mixins = {require("scripts/mixins/families/worm")}
require("scripts/globals/regimes")
-----------------------------------

function onMobDeath(mob, player, isKiller)
    tpz.regime.checkRegime(player, mob, 699, 1, tpz.regime.type.GROUNDS)
    tpz.regime.checkRegime(player, mob, 700, 1, tpz.regime.type.GROUNDS)
end
