-----------------------------------
-- Area: Boneyard_Gully
--  Mob: Shikaree X
-- TODO: Does not cast Familiar
-- TODO: Animation of re-summoning pet is wrong
-- TODO: Should hold TP until all 3 Shikaree have TP for a 3x WS
-- TODO: Chat messages for Starting a SC, Saving TP
-----------------------------------
local ID = require("scripts/zones/Boneyard_Gully/IDs")
mixins = {require("scripts/mixins/job_special")}
-----------------------------------

function onMobSpawn(mob)
    mob:useMobAbility(1017) -- Set mob to use Call Beast when spawned
end

function onMobFight(mob, target)
    mob:setMod(tpz.mod.REGAIN, 70) -- Took out 70 innate regain from modid causing it to spam WS before the fight started. Increased to 70 in LUA to compensate.
end
    
function onMobDeath(mob, player, isKiller)
    mob:showText(mob, ID.text.SHIKAREE_X_DEATH)
end

function onMobInitialize(mob)
	mob:addListener("WEAPONSKILL_STATE_ENTER", "WS_START_MSG", function(mob, wsid)
		if (wsid == 18) or (wsid == 23) or (wsid == 25) then
			mob:showText(mob, ID.text.SHIKAREE_X_WS3)
		end
	end)
end
