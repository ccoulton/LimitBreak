-----------------------------------
-- Area: Boneyard_Gully
--  Mob: Shikaree Z
-- TODO: Should hold TP until all 3 Shikaree have TP for a 3x WS
-- TODO: Chat messages for Starting a SC, Saving TP
-- TODO: Needs to cast Jump
-----------------------------------
local ID = require("scripts/zones/Boneyard_Gully/IDs")
-----------------------------------

function onMobEngaged(mob, target)
    mob:useMobAbility(732) -- Set mob to use Call Wyvern when engaged
    mob:showText(mob, ID.text.MITHRA_ENGAGE)
end

function onMobFight(mob, target)
    mob:setMod(tpz.mod.REGAIN, 70) -- Took out 70 innate regain from modid causing it to spam WS before the fight started. Increased to 70 in LUA to compensate.
end

function onMobInitialize(mob)
	mob:addListener("WEAPONSKILL_STATE_ENTER", "WS_START_MSG", function(mob, wsid)
		if (wsid == 116) or (wsid == 118) or (wsid == 119) or (wsid == 120) then
			mob:showText(mob, ID.text.SHIKAREE_Z_WS2)
		end
	end)
end

function onMobDeath(mob, player, isKiller)
    target:showText(mob, ID.text.SHIKAREE_Z_DEATH)
end
