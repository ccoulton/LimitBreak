-----------------------------------
-- Area: Behemoth's Dominion
--  HNM: King Behemoth
-----------------------------------
local ID = require("scripts/zones/Behemoths_Dominion/IDs")
mixins = {require("scripts/mixins/rage")}
require("scripts/globals/settings")
require("scripts/globals/status")
require("scripts/globals/titles")
require("scripts/globals/magic")
require("scripts/globals/mobs")
-----------------------------------

function onMobInitialize(mob)
end

function onMobSpawn(mob)
    print("POP")
    if LandKingSystem_NQ > 0 or LandKingSystem_HQ > 0 then
        GetNPCByID(ID.npc.BEHEMOTH_QM):setStatus(tpz.status.DISAPPEAR)
    end
    mob:setMobMod(tpz.mobMod.DRAW_IN, 1)
    mob:setMobMod(tpz.mobMod.DRAW_IN_CUSTOM_RANGE, 35)
    mob:setMobMod(tpz.mobMod.ADD_EFFECT, 1)
    mob:setLocalVar("[rage]timer", 3600) -- 60 minutes
    mob:setMobMod(tpz.mobMod.CLAIM_SHIELD, 1)
    mob:addMod(tpz.mod.ATT, -50)
    mob:setMod(tpz.mod.DOUBLE_ATTACK, 10)
    mob:setLocalVar("meteor", os.time())
end

function onMobFight(mob, target)
    if mob:getHPP() >= 50 then
        mob:setMod(tpz.mod.REGAIN, 150)
    elseif mob:getHPP() < 50 and mob:getHPP() > 25 then
        mob:setMod(tpz.mod.REGAIN, 100)
    else
        mob:setMod(tpz.mod.REGAIN, 80)
    end

    local meteor = mob:getLocalVar("meteor")
    local now = os.time()

    if now > (meteor + 40) then
        mob:castSpell(218)
        mob:setLocalVar("meteor", os.time())
    end

    -- The spell Meteor still requires 418 MP to start the precast function - this is here as a temp fix to never run out of MP
    if mob:getMP() < 500 then
        mob:setMP(1000)
    end

end

function onAdditionalEffect(mob, target, damage)
    local params = {}
    params.chance = 20
    params.duration = 5
    return tpz.mob.onAddEffect(mob, target, damage, tpz.mob.ae.STUN, params)
end

function onSpellPrecast(mob, spell)
    if spell:getID() == 218 then
        spell:setAoE(tpz.magic.aoe.RADIAL)
        spell:setFlag(tpz.magic.spellFlag.HIT_ALL)
        spell:setRadius(30)
        spell:setAnimation(280)
        spell:setMPCost(1)
    end
end

function onMobWeaponSkill(target, mob, skill)

end

function onMobDeath(mob, player, isKiller)
    player:addTitle(tpz.title.BEHEMOTH_DETHRONER)
end

function onMobDespawn(mob)
    -- Set King_Behemoth's Window Open Time
    if LandKingSystem_HQ ~= 1 then
        SetServerVariable("KingBehemothUP", 0)
        local wait = 72 * 3600
        SetServerVariable("[POP]King_Behemoth", os.time() + wait) -- 3 days
        if LandKingSystem_HQ == 0 then -- Is time spawn only
            DisallowRespawn(mob:getID(), true)
        end
    end

    -- Set Behemoth's spawnpoint and respawn time (21-24 hours)
    if LandKingSystem_NQ ~= 1 then
        SetServerVariable("[PH]King_Behemoth", 0)
        DisallowRespawn(ID.mob.BEHEMOTH, false)
        UpdateNMSpawnPoint(ID.mob.BEHEMOTH)
        local respawn = (75600 + ((math.random(0, 6)) * 1800)) -- 21 - 24 hours with half hour windows
        GetMobByID(ID.mob.BEHEMOTH):setRespawnTime(respawn)
        SetServerVariable("NQBehemothRespawn",(os.time() + respawn))
    end
end
