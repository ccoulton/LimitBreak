-----------------------------------
-- Area: The Sanctuary of Zi'Tah
--   NM: Guardian Treant
-- Involved in Quest: Forge Your Destiny
-----------------------------------

function onMobSpawn(mob)
	mob:setMod(tpz.mod.ACC, 175)
	mob:setMod(tpz.mod.ATT, 150)
end

function onMobDeath(mob, player, isKiller)
    player:setCharVar("ForgeYourDestiny_killed", 1)
end

