-----------------------------------
-- Area: Uleguerand Range
--  Mob: Father Frost
-----------------------------------
local ID = require("scripts/zones/Uleguerand_Range/IDs")
-----------------------------------
function onMobSpawn(mob, player, isKiller)
	mob:setMobMod(tpz.mobMod.AUTO_SPIKES, 1)
	mob:addStatusEffect(tpz.effect.ICE_SPIKES, 50, 0, 0)
	mob:getStatusEffect(tpz.effect.ICE_SPIKES):setFlag(tpz.effectFlag.DEATH)
end

function onSpikesDamage(mob, target, damage)
    local INT_diff = mob:getStat(tpz.mod.INT) - target:getStat(tpz.mod.INT)

    if INT_diff > 20 then
        INT_diff = 20 + (INT_diff - 20) * 0.5 -- INT above 20 is half as effective.
    end

    local dmg = (damage + INT_diff) * 0.5 -- INT adjustment and base damage averaged together.
    local params = {}
    params.bonusmab = 0
    params.includemab = false
    dmg = addBonusesAbility(mob, tpz.magic.ele.ICE, target, dmg, params)
    dmg = dmg * applyResistanceAddEffect(mob, target, tpz.magic.ele.ICE, 0)
    dmg = adjustForTarget(target, dmg, tpz.magic.ele.ICE)
    dmg = finalMagicNonSpellAdjustments(mob, target, tpz.magic.ele.ICE, dmg)

    if dmg < 0 then
        dmg = 0
    end

    return tpz.subEffect.ICE_SPIKES, tpz.msg.basic.SPIKES_EFFECT_DMG, dmg
end

function onMobDeath(mob, player, isKiller)
end

function onMobDespawn(mob)
    local phIndex = mob:getLocalVar("phIndex")
    local ph = GetMobByID(ID.mob.SNOW_MAIDEN_PH[phIndex])
  
	DisallowRespawn(ID.mob.FATHER_FROST, true)
	DisallowRespawn(ID.mob.SNOW_MAIDEN, true)
    phIndex = (phIndex % 3) + 1
    ph = GetMobByID(ID.mob.SNOW_MAIDEN_PH[phIndex])
    ph:setLocalVar("timeToGrow", os.time() + math.random(86400, 108000)) -- 24 to 30 hours 
 	ph:setLocalVar("phIndex", phIndex)
end