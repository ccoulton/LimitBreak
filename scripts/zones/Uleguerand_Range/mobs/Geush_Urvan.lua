-----------------------------------
-- Area: Uleguerand Range
--  HNM: Geush Urvan
-----------------------------------
require("scripts/globals/status")
-----------------------------------
function onMobInitialize(mob)
	mob:SetMobSkillAttack(1200)
end

function onMobSpawn(mob)
	mob:SetMagicCastingEnabled(false) -- Just so he doesn't casts magic on spawn
	mob:setMod(tpz.mod.REGAIN, 200)
end

function onMobWeaponSkill(target, mob, skill)
	mob:SetMagicCastingEnabled(true)
end