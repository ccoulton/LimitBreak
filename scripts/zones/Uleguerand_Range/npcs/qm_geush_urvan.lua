-----------------------------------
--  Area: Uleguerand_Range
--   NPC: ??? (Spawns Geush Urvan)
-----------------------------------
local ID = require("scripts/zones/Uleguerand_Range/IDs")
require("scripts/globals/npc_util")
-----------------------------------

function onTrade(player, npc, trade)
    if npcUtil.tradeHas(trade, 1824) and npcUtil.popFromQM(player, npc, ID.mob.GEUSH_URVAN, {hide = 0}) then -- Haunted Muleta
        player:confirmTrade()
		player:messageSpecial(ID.text.HUGE_BEAST, 1824)
    end
end

function onTrigger(player, npc)
	player:messageSpecial(ID.text.SCRAP_OF_CLOTH)
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)
end
