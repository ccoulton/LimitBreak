-----------------------------------
--
-- Zone: Qulun_Dome (148)
--
-----------------------------------
local ID = require("scripts/zones/Qulun_Dome/IDs")
require("scripts/globals/conquest")
-----------------------------------

function onInitialize(zone)
    local ZA = GetServerVariable("Zadha_Adamantking_UP")
    local zare = GetServerVariable("Zadha_Adamantking_Respawn")
    local dqre = GetServerVariable("Diamond_Quadav_Respawn")

    if ZA == 1 then
--[[    UpdateNMSpawnPoint(ID.mob.ZADHA_ADAMANTKING)
        if os.time() < zare then
        GetMobByID(ID.mob.ZADHA_ADAMANTKING):setRespawnTime(zare - os.time())
        else
            GetMobByID(ID.mob.ZADHA_ADAMANTKING):setRespawnTime(300)
        end
    else--]]
        UpdateNMSpawnPoint(ID.mob.DIAMOND_QUADAV)
        if os.time() < dqre then
            GetMobByID(ID.mob.DIAMOND_QUADAV):setRespawnTime(dqre - os.time())
        else
            GetMobByID(ID.mob.DIAMOND_QUADAV):setRespawnTime(300)
        end
    end
end

function onZoneIn(player, prevZone)
    local cs = -1
    if player:getXPos() == 0 and player:getYPos() == 0 and player:getZPos() == 0 then
        player:setPos(337.901, 38.091, 20.087, 129)
    end
    return cs
end

function onConquestUpdate(zone, updatetype)
    tpz.conq.onConquestUpdate(zone, updatetype)
end

function onRegionEnter(player, region)
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)
end
