require("scripts/globals/mixins")
require("scripts/globals/status")
-----------------------------------

g_mixins = g_mixins or {}
g_mixins.families = g_mixins.families or {}

g_mixins.families.imp = function(mob)
    mob:setMobMod(tpz.mobMod.NO_STANDBACK, 1)
    -- 20% chance to break horn on critical hit
    mob:addListener("CRITICAL_TAKE", "IMP_CRITICAL_TAKE", function(mob)
        if math.random(100) < 20 and mob:AnimationSub() == 0 then
            mob:AnimationSub(1)
        end
    end)

end

return g_mixins.families.imp
